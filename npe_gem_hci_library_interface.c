//  Copyright (c) 2017-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "npe_error_code.h"
#include "npe_log.h"

#include "wf_gem_hci_comms.h"
#include "wf_gem_hci_manager.h"
#include "wf_gem_hci_manager_gymconnect.h"
#include "npe_gem_hci_serial_interface.h"
#include "npe_gem_hci_library_interface.h"

#include "serial_dfu_host/UartSecureDFU/UartSecureDFU.h"
#include "base64.h"



typedef void (*npe_gem_fp)(void); // Generic function pointer





/** @brief Describes a GEM message in concrete format 
 *
 */
typedef struct 
{
    uint8_t message_class_id;
    uint8_t message_id;
    npe_inc_function_args args;
} npe_hci_function_info_type;


static standard_response_t m_last_response;          // Last response received from GEM
static wf_gem_hci_comms_message_t receivedMessage;  // Last received message from GEM
static npe_hci_function_info_type messageToSend;    // Message to send to GEM
static one_second_timeout_t m_timeout_cb;           // Function to call on 1 second timeout.
static on_gem_event_cb_t m_event_cb;                // Function to call on GEM system event.

#define TRIGGER_EVENT    _wf_gem_hci_manager_trigger_event
void _wf_gem_hci_manager_trigger_event(uint8_t message_class_id, uint8_t message_id, gem_event_args_t *p_data);

/** @brief Called by RX THREAD. Parses received bytes from GEM. 
 *
 */
static void npe_gem_hci_library_interface_parse_bytes(uint8_t* byte_buff, int byte_num)
{
    for(int i = 0; i < byte_num;i++)
    {
        wf_gem_hci_comms_process_rx_byte(byte_buff[i]);
    }
}

/** @brief Called by TX THREAD. Sends a HCI ANT Config Message to GEM. 
 *
 */
static void npe_hci_library_send_ant_receiver_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_START_DISCOVERY:
        {
            wf_gem_hci_manager_send_command_ant_receiver_start_discovery(
                messageToSend.args.ant_receiver_start_discovery.ant_plus_device_profile, 
                messageToSend.args.ant_receiver_start_discovery.prximity_bin, 
                messageToSend.args.ant_receiver_start_discovery.discovery_timeout);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_STOP_DISCOVERY:
        {
            wf_gem_hci_manager_send_command_ant_receiver_stop_discovery(
                messageToSend.args.ant_receiver_stop_discovery.ant_plus_device_profile);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_CONNECT_DEVICE:
        {
            wf_gem_hci_manager_send_command_ant_receiver_connect_device(
                messageToSend.args.ant_receiver_connect_device.ant_plus_device_profile, 
                messageToSend.args.ant_receiver_connect_device.device_number,
                messageToSend.args.ant_receiver_connect_device.proximity_bin,
                messageToSend.args.ant_receiver_connect_device.connection_timeout);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_DISCONNECT_DEVICE:
        {
            wf_gem_hci_manager_send_command_ant_receiver_disconnect_device(
                messageToSend.args.ant_receiver_disconnect_device.ant_plus_device_profile);
            break;
        }

        case WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_REQUEST_CALIBRATION:
        {
            wf_gem_hci_manager_send_command_ant_receiver_request_calibration(
                messageToSend.args.ant_receiver_request_calibration.ant_plus_device_profile,
                messageToSend.args.ant_receiver_request_calibration.attempts);
            break;
        }

        case WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_MANUFACTURER_INFO:
        {
            wf_gem_hci_manager_send_command_ant_receiver_get_manufacturer_info(
                messageToSend.args.ant_receiver_get_manufacturer_info.ant_plus_device_profile);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_BATTERY_STATUS:
        {
            wf_gem_hci_manager_send_command_ant_receiver_request_battery(
                messageToSend.args.ant_receiver_request_battery_status.ant_plus_device_profile,
                messageToSend.args.ant_receiver_request_battery_status.data.battery_index);
            break;
        }
        default:
        {
            break;
        }
    }
}

/** @brief Called by TX THREAD. Sends a HCI NFC Reader Message to GEM. 
 *
 */
static void npe_hci_library_send_nfc_reader_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_NFC_READER_ENABLE_RADIO:
        {
            wf_gem_hci_manager_send_command_nfc_reader_enable_radio();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_NFC_READER_DISABLE_RADIO:
        {
            wf_gem_hci_manager_send_command_nfc_reader_disable_radio();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SCAN_PERIOD:
        {
            wf_gem_hci_manager_send_command_nfc_reader_set_scan_period(messageToSend.args.nfc_reader_set_scan_period.scan_period_in_milliseconds);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SCAN_PERIOD:
        {
            wf_gem_hci_manager_send_command_nfc_reader_get_scan_period();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SUPPORTED_TAGS_TYPES:
        {
            wf_gem_hci_manager_send_command_nfc_reader_set_supported_tag_types(messageToSend.args.nfc_reader_set_tag_types.tag_types_bitfield);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SUPPORTED_TAGS_TYPES:
        {
            wf_gem_hci_manager_send_command_nfc_reader_get_supported_tag_types();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_NFC_RADIO_TEST_MODE:
        {
            wf_gem_hci_manager_send_command_nfc_reader_set_radio_test_mode(messageToSend.args.nfc_reader_set_radio_test_mode.mode);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_SET_NFC_TAG_CONFIGURATION:
        {
            wf_gem_hci_manager_send_command_nfc_reader_set_tag_board_configuration(&messageToSend.args.nfc_reader_set_tag_board_config.config);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_GET_NFC_TAG_CONFIGURATION:
        {
            wf_gem_hci_manager_send_command_nfc_reader_get_tag_board_configuration();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_COMMUNICATION_CHECK :
        {
            wf_gem_hci_manager_send_command_nfc_reader_tag_board_comms_check();
            break;
        }
        default:
        {
            break;
        }
    }
}


/** @brief Called by TX THREAD. Sends a HCI ANT Control Message to GEM. 
 *
 */
static void npe_hci_library_send_ant_control_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_ENABLED:
        {
            wf_gem_hci_manager_send_command_ant_control_get_ant_profile_enable(messageToSend.args.ant_control_ant_profile_enable.profile);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_ENABLED:
        {
            wf_gem_hci_manager_send_command_ant_control_set_ant_profile_enable(messageToSend.args.ant_control_ant_profile_enable.profile, messageToSend.args.ant_control_ant_profile_enable.enable_or_disable);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_FREQ_DIV:
        {
            wf_gem_hci_manager_send_command_ant_control_get_ant_profile_frequency_diversity(messageToSend.args.ant_control_frequency_diversity_enable.profile);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_FREQ_DIV:
        {
            wf_gem_hci_manager_send_command_ant_control_set_ant_profile_frequency_diversity(
                messageToSend.args.ant_control_frequency_diversity_enable.profile,
                messageToSend.args.ant_control_frequency_diversity_enable.mode,
                messageToSend.args.ant_control_frequency_diversity_enable.channel_a_frequency_index,
                messageToSend.args.ant_control_frequency_diversity_enable.channel_b_frequency_index); 
            break;
        }
        default:
        {
            break;
        }
    }
}

      
/** @brief Called by TX THREAD. Sends a HCI ANT Config Message to GEM. 
 *
 */
static void npe_hci_library_send_ant_config_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_HW_VER:
        {
            wf_gem_hci_manager_send_command_ant_config_get_hardware_version();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_HW_VER:
        {
            wf_gem_hci_manager_send_command_ant_config_set_hardware_version(messageToSend.args.ant_config_set_hardware_version.hardware_version);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MANU_ID:
        {
            wf_gem_hci_manager_send_command_ant_config_set_model_number(messageToSend.args.ant_config_set_model_number.model_number);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_MANU_ID:
        {
            wf_gem_hci_manager_send_command_ant_config_get_model_number();
            break;
        }
        wf_gem_hci_manager_send_command_ant_config_set_manufacturer_id(messageToSend.args.ant_config_manufacturer_id.manufacturer_id);
        case WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MODEL_NUM:
        {
            wf_gem_hci_manager_send_command_ant_config_set_model_number(messageToSend.args.ant_config_set_model_number.model_number);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SW_VER:
        {
            uint8_t main_version = messageToSend.args.ant_config_set_version.main;
            uint8_t supplemental = messageToSend.args.ant_config_set_version.supplemental;
            wf_gem_hci_manager_send_command_ant_config_set_software_version(main_version, supplemental);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SERIAL_NUMBER:
        {
            wf_gem_hci_manager_send_command_ant_config_set_serial_number(messageToSend.args.ant_config_set_serial_number.serial_number);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_ANT_DEVICE_NUMBER:
        {
            wf_gem_hci_manager_send_command_ant_config_set_ant_device_number(   messageToSend.args.ant_config_set_device_number.device_number_type, 
                                                                                messageToSend.args.ant_config_set_device_number.device_number);
            break;
        }

        case WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_ANT_DEVICE_NUMBER:
        {
            wf_gem_hci_manager_send_command_ant_config_get_ant_device_number();
            break;
        }
        
        default:
        {
            break;
        }
    }
}

/** @brief Called by TX THREAD. Sends a HCI Gymconnect Message to GEM. 
 *
 */
static void npe_hci_library_send_gymconnect_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_CONTROL_FEATURES:
        {
            wf_gem_hci_manager_gymconnect_set_supported_equipment_control_features(messageToSend.args.gymconnect_set_supported_equipment_control_features.equipment_control_field_identifier);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TYPE:
        {
            wf_gem_hci_manager_gymconnect_set_fe_type(messageToSend.args.gymconnect_set_fe_type.fe_type);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_STATE:
        {
            wf_gem_hci_manager_gymconnect_set_fe_state(messageToSend.args.gymconnect_set_fe_state.fe_state);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_UPDATE_WORKOUT_DATA:
        {
            wf_gem_hci_manager_gymconnect_perform_workout_data_update();
            break;
        }
        default:
        {
            break;
        }
    }
}

/** @brief Called by TX THREAD. Sends a HCI Bluetooth Control Message to GEM. 
 *
 */
static void npe_hci_library_send_bt_control_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_BT_CONTROL_START_ADV:
        {
            wf_gem_hci_manager_send_command_bluetooth_control_start_advertising();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_CONTROL_STOP_ADV:
        {
            wf_gem_hci_manager_send_command_bluetooth_control_stop_advertising();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_CONTROL_DISCONNECT_CENTRAL:
        {
            wf_gem_hci_manager_send_command_bluetooth_control_disconnect_central();
            break;
        }
        default:
        {
            break;
        }
    }
}

/** @brief Called by TX THREAD. Sends a HCI Bluetooth Receiver Message to GEM. 
 *
 */
static void npe_hci_library_send_bt_receiver_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_START_DISCOVERY:
        {
            wf_gem_hci_manager_send_command_bluetooth_receiver_start_discovery(
                messageToSend.args.bt_receiver_discovery.service_id,
                messageToSend.args.bt_receiver_discovery.minimum_rssi,
                messageToSend.args.bt_receiver_discovery.discovery_timeout);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_STOP_DISCOVERY:
        {
            wf_gem_hci_manager_send_command_bluetooth_receiver_stop_discovery(messageToSend.args.bt_receiver_stop_discovery.service_id);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_START_DISCOVERY_IBEACON:
        {
            wf_gem_hci_manager_send_command_bluetooth_receiver_start_discovery_ibeacon(messageToSend.args.bt_receiver_start_discovery_ibeacon.company_id, 
                messageToSend.args.bt_receiver_start_discovery_ibeacon.uuid, 
                messageToSend.args.bt_receiver_start_discovery_ibeacon.min_rssi,
                messageToSend.args.bt_receiver_start_discovery_ibeacon.discovery_timeout);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_CONNECT_DEVICE:
        {
            wf_gem_hci_manager_send_command_bluetooth_receiver_connect_device(
                messageToSend.args.bt_receiver_connect_device.service_id,
                messageToSend.args.bt_receiver_connect_device.bluetooth_address,
                messageToSend.args.bt_receiver_connect_device.connection_timeout,
                messageToSend.args.bt_receiver_connect_device.use_gymconnect_events
                );
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_DISCONNECT_DEVICE:
        {
            wf_gem_hci_manager_send_command_bluetooth_receiver_disconnect_device(
                messageToSend.args.bt_receiver_disconnect_device.service_id);
            break;
        }
        
        default:
        {
            break;
        }
    }
}


/** @brief Called by TX THREAD. Sends a HCI Bluetooth Device Info Message to GEM. 
 *
 */
static void npe_hci_library_send_bt_device_info_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MANU_NAME:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_set_manufacturer_name(messageToSend.args.bt_device_info_set_manufacturer_name.manufacturer_name);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MODEL_NUM:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_set_model_number(messageToSend.args.bt_device_info_set_model_number.model_number);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_SERIAL_NUM:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_set_serial_number(messageToSend.args.bt_device_info_set_serial_number.serial_number);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_HW_REV:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_set_hardware_rev(messageToSend.args.bt_device_info_set_hardware_revision.hardware_revision);
            break;
        } 
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_FW_REV:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_set_firmware_rev(messageToSend.args.bt_device_info_set_firmware_revision.firmware_revision);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BATT_SERV_INC:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_set_battery_included(messageToSend.args.bt_device_info_set_battery_included.battery_included);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_PNP_ID:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_set_pnp_id(
                messageToSend.args.bt_device_info_set_pnp_id.vendor_source_id, 
                messageToSend.args.bt_device_info_set_pnp_id.vendor_id,
                messageToSend.args.bt_device_info_set_pnp_id.product_id,
                messageToSend.args.bt_device_info_set_pnp_id.product_version);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_PNP_ID:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_get_pnp_id();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_BSCS_SERVICE_ENABLED:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_get_bscs_service_included();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BSCS_SERVICE_ENABLED:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_set_bscs_service_included(messageToSend.args.bt_device_info_bscs_service_set_enabled.enabled);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_POWER_SERVICE_ENABLED:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_get_power_service_included();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_POWER_SERVICE_ENABLED:
        {
            wf_gem_hci_manager_send_command_bluetooth_info_set_power_service_included(messageToSend.args.bt_device_info_power_service_set_enabled.enabled);
            break;
        }

    }
}

/** @brief Called by TX THREAD. Sends a HCI Bluetooth Config Message to GEM. 
 *
 */
static void npe_hci_library_send_bt_config_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_DEVICE_NAME:
        {
            wf_gem_hci_manager_send_command_bluetooth_config_set_device_name(messageToSend.args.bt_config_set_device_name.bluetooth_name);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_ADV_TIMING:
        {
            wf_gem_hci_manager_send_command_bluetooth_config_get_advertising_rates_and_timeouts();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_ADV_TIMING:
        {
            
            wf_gem_hci_manager_send_command_bluetooth_config_set_advertising_rates_and_timeouts(
                messageToSend.args.bt_config_set_adv_interval_and_timeouts.fast_advertising_interval, messageToSend.args.bt_config_set_adv_interval_and_timeouts.fast_advertising_timeout,
                messageToSend.args.bt_config_set_adv_interval_and_timeouts.normal_advertising_interval, messageToSend.args.bt_config_set_adv_interval_and_timeouts.normal_advertising_timeout);
            break;
        }
    }
}

/** @brief Called by TX THREAD. Sends a HCI System Message to GEM. 
 *
 */
static void npe_hci_library_send_system_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_SYSTEM_PING:
        {

            wf_gem_hci_manager_send_command_sytem_ping();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_SYSTEM_GET_GEM_MODULE_VERSION_INFO:
        {
            wf_gem_hci_manager_send_command_sytem_get_gem_module_version_information();
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_SYSTEM_INITIATE_BOOTLOADER:
        {
            wf_gem_hci_manager_send_command_sytem_initiate_bootloader(messageToSend.args.system_initiate_bootloader.bootloader_mode);
            break;
        }
    }
}

/** @brief Called by TX THREAD. Sends a HCI Hardware Message to GEM. 
 *
 */
static void npe_hci_library_send_hardware_message(uint8_t message_id)
{
    switch(message_id)
    {
        case WF_GEM_HCI_COMMAND_ID_HARDWARE_SET_PIN_IO_CONFIG :
        {

            wf_gem_hci_manager_send_command_hardware_set_pin(messageToSend.args.hw_pins.number_of_pins, (uint8_t*)messageToSend.args.hw_pins.pin_config);
            break;
        }
        case WF_GEM_HCI_COMMAND_ID_HARDWARE_GET_PIN_IO_CONFIG :
        {

            wf_gem_hci_manager_send_command_hardware_get_pin();
            break;
        }
        default:
        {
            LOGI("Message not support to TX\n");
            break;
        }
    }
}





/** @brief Called by TX THREAD. Sends a HCI message to GEM. This should be 
 * called only once local struct messageToSend has been populated. 
 *
 */
static void npe_hci_library_send_message(void)
{
    switch(messageToSend.message_class_id)
    {
        case WF_GEM_HCI_MSG_CLASS_SYSTEM:
        {
            npe_hci_library_send_system_message(messageToSend.message_id);
            break;
        }
        case WF_GEM_HCI_MSG_CLASS_HARDWARE:
        {
            npe_hci_library_send_hardware_message(messageToSend.message_id);
            break;
        }
        case WF_GEM_HCI_MSG_CLASS_BT_CONFIG:
        {
            npe_hci_library_send_bt_config_message(messageToSend.message_id);
            break;
        }
        case WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO:
        {
            npe_hci_library_send_bt_device_info_message(messageToSend.message_id);
            break;
        }
        case WF_GEM_HCI_MSG_CLASS_BT_CONTROL:
        {
            npe_hci_library_send_bt_control_message(messageToSend.message_id);
            break;
        }
        case WF_GEM_HCI_MSG_CLASS_BT_RECEIVER:
        {
            npe_hci_library_send_bt_receiver_message(messageToSend.message_id);
            break;
        }

        case WF_GEM_HCI_MSG_CLASS_GYM_CONNECT:
        {
            npe_hci_library_send_gymconnect_message(messageToSend.message_id);
            break;

        }
        case WF_GEM_HCI_MSG_CLASS_ANT_CONTROL:
        {
            npe_hci_library_send_ant_control_message(messageToSend.message_id);
            break;
        }
        case WF_GEM_HCI_MSG_CLASS_ANT_CONFIG:
        {
            npe_hci_library_send_ant_config_message(messageToSend.message_id);
            break;
        }
        case WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER:
        {
            npe_hci_library_send_ant_receiver_message(messageToSend.message_id);
            break;
        }
        case WF_GEM_HCI_MSG_CLASS_NFC_READER:
        {
            npe_hci_library_send_nfc_reader_message(messageToSend.message_id);
            break;
        }
        default:
        {
            break;
        }
    }
}

/** @brief CALLED BY 1 SECOND TIMER thread and calls application func
 *
 * @return  ::true if response we are waiting on has been recieved
 * 
 */
static void npe_hci_library_timeout(void)
{
    if(m_timeout_cb)
    {
        m_timeout_cb();
    }
}

static void trigger_event(gem_event_t* event) {
    if(m_event_cb)
    {
        m_event_cb(event);
    }
}

/** @brief CALLED BY RX THREAD to copy received message to local structure.
 *
 * @param[in] message is a pointer to the received message.
 * @param[in] size is a 4-byte unsigned value denoting the size of the recieved message in bytes.
 */
static void npe_gem_hci_library_process_received_msg(void* message, uint32_t size)
{
    // TO DO - ASSERT SIZE DOES NOT EXCEED LOCAL BUFFER
    memcpy(&receivedMessage, message, size);
}

/** @brief CALLED BY RX THREAD to check if RX condition has been met.
 *
 * @return  ::true if response we are waiting on has been recieved
 * 
 */
static bool npe_gem_library_check_if_response_received(void)
{
    if(receivedMessage.message_event_flag == false && receivedMessage.message_class_id == messageToSend.message_class_id && receivedMessage.message_id == messageToSend.message_id)
    {

        return true;
    }
    
    return false;
}

/** @brief Initializes the NPE GEM HCI library and serial interface.
 *
 * @param[in] p_comport is a string denoting the name of the port to open.
 * @param[in] one_second_timeout_cb is an unsigned char (1 octet) denoting the channel number to query.
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_SERIAL_NO_COMPORT
 *          ::NPE_GEM_RESPONSE_SERIAL_OPEN_FAIL
 *          ::NPE_GEM_RESPONSE_SERIAL_CONFIG_FAIL
 */
uint32_t npe_gem_hci_library_interface_init(const char* p_comport, one_second_timeout_t one_second_timout_cb, on_gem_event_cb_t gem_event_cb)
{
    npe_serial_interface_callbacks_t tx_callbacks;
    tx_callbacks.parse_bytes_cb = npe_gem_hci_library_interface_parse_bytes;
    tx_callbacks.transmit_message_cb = npe_hci_library_send_message;
    tx_callbacks.timeout_cb = npe_hci_library_timeout;
    tx_callbacks.retry_timeout_cb = wf_gem_hci_manager_process_command_retry;

    if(one_second_timout_cb)
        m_timeout_cb = one_second_timout_cb;

    if(gem_event_cb) {
        m_event_cb = gem_event_cb;
    }

    // Initialize the serial interface
    npe_serial_interface_list_ports();
    return(npe_serial_interface_init(p_comport, &tx_callbacks));

}

void npe_gem_hci_library_interface_shutdown()
{
    npe_serial_interface_shutdown();

    // TODO Cleanup
}

/** @brief Send Ping command to GEM.
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_ping(void)
{
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_SYSTEM;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_SYSTEM_PING;

    // Start send message then wait for response.
    if(locked) 
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_sytem_ping();  
    
    return(npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 1));
}

/** @brief Send Set Pin command to the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_hardware_set_pin(uint8_t number_of_pins, npe_hci_pin_t p_pin_settings[], standard_response_t* p_set_device_name_response)
{
    if(number_of_pins > MAX_PINS_ALLOWED || number_of_pins == 0)
        return(NPE_GEM_RESPONSE_INVALID_PARAMETER);
    
    
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_HARDWARE;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_HARDWARE_SET_PIN_IO_CONFIG;
    messageToSend.args.hw_pins.number_of_pins = number_of_pins;

    for(int i = 0; i < number_of_pins; i++)
    {
        messageToSend.args.hw_pins.pin_config[i].pin_number = p_pin_settings[i].pin_number;
        messageToSend.args.hw_pins.pin_config[i].pin_io_mode = p_pin_settings[i].pin_io_mode;
    }

    // Start send message then wait for response.
    if(locked) 
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_hardware_set_pin(messageToSend.args.hw_pins.number_of_pins, (uint8_t*)messageToSend.args.hw_pins.pin_config);  


    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_HARDWARE;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_HARDWARE_SET_PIN_IO_CONFIG;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_set_device_name_response->error_code = m_last_response.error_code;
    }
    return(res);

}

/** @brief Send Get Pin command to the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_hardware_get_pin(standard_response_t* p_response)
{
   
    
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_HARDWARE;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_HARDWARE_GET_PIN_IO_CONFIG;


    // Start send message then wait for response.
    if(locked) 
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_hardware_get_pin(); 

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_HARDWARE;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_HARDWARE_GET_PIN_IO_CONFIG;
    m_last_response.error_code = 0;
    m_last_response.args.hw_get_pins.number_of_pins = 1;
    npe_hci_pin_t pin;
    pin.pin_number = 1;
    pin.pin_io_mode = 0;
    m_last_response.args.hw_get_pins.pin_config[0] = pin;
#endif

    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    

    return(res);
}

/** @brief Send Get Version command to the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_system_get_version(standard_response_t* p_response)
{
   
    // Check if we are on a different thread. Lock if we are.
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_SYSTEM;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_SYSTEM_GET_GEM_MODULE_VERSION_INFO;


    // Start send message then wait for response.
    if(locked) 
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_sytem_get_gem_module_version_information(); 

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_SYSTEM;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_SYSTEM_GET_GEM_MODULE_VERSION_INFO;
    m_last_response.error_code = 0;
    m_last_response.args.gem_version.bl_version_device_variant = 0;
    m_last_response.args.gem_version.bl_version_major = 0;
    m_last_response.args.gem_version.bl_version_revision = 0;
    m_last_response.args.gem_version.bl_version_vendor = 0;
    m_last_response.args.gem_version.fw_version_build = 0;
    m_last_response.args.gem_version.fw_version_major = 0;
    m_last_response.args.gem_version.bl_version_device_variant = 0;
    m_last_response.args.gem_version.fw_version_minor = 0;
    m_last_response.args.gem_version.fw_version_simple = 0;
    m_last_response.args.gem_version.hw_version = 0;
    m_last_response.args.gem_version.product_id = 0;
    m_last_response.args.gem_version.vendor_id = 0;
#endif

    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    

    return(res);
}
/** @brief Send Get Version command to the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_system_initiate_bootloader(uint8_t bootloader_mode, standard_response_t* p_response)
{
   
    // Check if we are on a different thread. Lock if we are.
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_SYSTEM;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_SYSTEM_INITIATE_BOOTLOADER;
    messageToSend.args.system_initiate_bootloader.bootloader_mode = bootloader_mode;

    // Start send message then wait for response.
    if(locked) 
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_sytem_initiate_bootloader(bootloader_mode); 

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_SYSTEM;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_SYSTEM_INITIATE_BOOTLOADER;
    m_last_response.error_code = 0;

#endif

    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    

    return(res);
}



/** @brief Send the Bluetooth Device Name to the GEM.
 *
 * @param[in] bluetooth_name is a string denoting the bluetooth device name.
 * @param[out] p_set_device_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_config_set_device_name(utf8_data_t* bluetooth_name, standard_response_t* p_set_device_name_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_DEVICE_NAME;
    messageToSend.args.bt_config_set_device_name.bluetooth_name = bluetooth_name;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_config_set_device_name(messageToSend.args.bt_config_set_device_name.bluetooth_name);
 
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_DEVICE_NAME;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_set_device_name_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Get the Bluetooth Advertising Interval and Timeouts
 *
 * @param[out] p_set_device_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_config_get_advertising_intervals_and_timeouts(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_ADV_TIMING;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_config_get_advertising_rates_and_timeouts();
 
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_ADV_TIMING;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Set the Bluetooth Advertising Interval and Timeouts
 *
 * @param[in] fast_advertising_interval is the fast advertising interval in units of 625us (max is 16384).
 * @param[in] fast_advertising_timeout is the fast advertising timeout in ms. 0 disables timeout
 * @param[in] normal_advertising_interval is the normal advertising interval in units of 625us (max is 16384).
 * @param[in] normal_advertising_timeout is the normal advertising timeout in ms. 0 disables timeout
 * @param[out] p_set_device_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_config_set_advertising_intervals_and_timeouts(
    uint16_t fast_advertising_interval, uint16_t fast_advertising_timout, 
    uint16_t normal_advertising_interval, uint16_t normal_advertising_timeout,
    standard_response_t* p_set_device_name_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_ADV_TIMING;
    messageToSend.args.bt_config_set_adv_interval_and_timeouts.fast_advertising_interval = fast_advertising_interval;
    messageToSend.args.bt_config_set_adv_interval_and_timeouts.fast_advertising_timeout = fast_advertising_timout;
    messageToSend.args.bt_config_set_adv_interval_and_timeouts.normal_advertising_interval = normal_advertising_interval;
    messageToSend.args.bt_config_set_adv_interval_and_timeouts.normal_advertising_timeout = normal_advertising_timeout;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_config_set_advertising_rates_and_timeouts(fast_advertising_interval, fast_advertising_timout, normal_advertising_interval, normal_advertising_timeout);
 
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_ADV_TIMING;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_set_device_name_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Send the Bluetooth Manufacturer Name to the GEM.
 *
 * @param[in] manufacturer_name is a string denoting the manufacturer name.
 * @param[out] p_set_manufacturer_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_manufacturer_name(utf8_data_t* manufacturer_name, standard_response_t* p_set_manufacturer_name_response)
{
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MANU_NAME;
    messageToSend.args.bt_device_info_set_manufacturer_name.manufacturer_name = manufacturer_name;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_set_manufacturer_name(messageToSend.args.bt_device_info_set_manufacturer_name.manufacturer_name);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MANU_NAME;
#endif

    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_set_manufacturer_name_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Send the Bluetooth Model Number to the GEM.
 *
 * @param[in] model_number is a string denoting the model number.
 * @param[out] p_set_manufacturer_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_model_number(utf8_data_t* model_number, standard_response_t* p_set_model_number_response)
{
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MODEL_NUM;
    messageToSend.args.bt_device_info_set_model_number.model_number = model_number;
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_set_model_number(messageToSend.args.bt_device_info_set_model_number.model_number);
 
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_MODEL_NUM;
#endif

    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_set_model_number_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Send the Bluetooth Serial Number to the GEM.
 *
 * @param[in] serial_number is a string denoting the serial number.
 * @param[out] p_response_error_code is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_serial_number(utf8_data_t* serial_number, standard_response_t* p_set_serial_number_response)
{
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_SERIAL_NUM;
    messageToSend.args.bt_device_info_set_serial_number.serial_number = serial_number;
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_set_serial_number(messageToSend.args.bt_device_info_set_serial_number.serial_number);
 
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_SERIAL_NUM;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_set_serial_number_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Send the Bluetooth Hardware Revision to the GEM.
 *
 * @param[in] hardware_revision is a string denoting the hardware revision.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_hardware_rev(utf8_data_t* hardware_revision, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_HW_REV;
    messageToSend.args.bt_device_info_set_hardware_revision.hardware_revision = hardware_revision;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else 
        wf_gem_hci_manager_send_command_bluetooth_info_set_hardware_rev(messageToSend.args.bt_device_info_set_hardware_revision.hardware_revision);
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_HW_REV;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Send the Bluetooth Firmware Revision to the GEM.
 *
 * @param[in] firmware_revision is a string denoting the firmware revision number.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_firmware_rev(utf8_data_t* firmware_revision, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_FW_REV;
    messageToSend.args.bt_device_info_set_firmware_revision.firmware_revision = firmware_revision;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else 
        wf_gem_hci_manager_send_command_bluetooth_info_set_firmware_rev(messageToSend.args.bt_device_info_set_firmware_revision.firmware_revision);
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_FW_REV;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Send whether battery service should be included to the GEM.
 *
 * @param[in] battery_included 1 byte unsigned value denoting whether to enable battery service.
 *            WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_INCLUDE (0x01)
 *            WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_NOT_INCLUDE (0x00)
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_battery_included(uint8_t battery_included, standard_response_t* p_response)
{
    if(battery_included != WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_NOT_INCLUDE &&
        battery_included != WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_INCLUDE)
    {
       return(NPE_GEM_RESPONSE_INVALID_PARAMETER); 
    }

    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BATT_SERV_INC;
    messageToSend.args.bt_device_info_set_battery_included.battery_included = battery_included;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_set_battery_included(messageToSend.args.bt_device_info_set_battery_included.battery_included);
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BATT_SERV_INC;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Sets the PnP ID on the GEM.
 *
 * @param[in] vendor_source_id 1 byte unsigned value denoting whether id is BT SIG or USB forum assigned.
 *            WF_GEM_HCI_BLUETOOTH_PNP_ID_SIG_COMPANY_ASSIGNED_NUMBER (0x01)
 *            WF_GEM_HCI_BLUETOOTH_PNP_ID_USB_FORUM_ASSIGNED_NUMBER (0x02)
 * @param[in] vendor_id 2 byte unsigned value denoting company's assigned Bluetooth SIG or USB forum assigned vendor ID.
 * @param[in] product_id 2 byte unsigned value denoting company's product ID.
 * @param[in] product_version 2 byte unsigned value denoting company's product version number.
 * 
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_pnp_id(
    uint8_t vendor_source_id,
    uint16_t vendor_id,
    uint16_t product_id,
    uint16_t product_version, 
    standard_response_t* p_response)
{


    if(vendor_source_id != WF_GEM_HCI_BLUETOOTH_PNP_ID_SIG_COMPANY_ASSIGNED_NUMBER &&
        vendor_source_id != WF_GEM_HCI_BLUETOOTH_PNP_ID_USB_FORUM_ASSIGNED_NUMBER)
    {
       return(NPE_GEM_RESPONSE_INVALID_PARAMETER); 
    }

    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_PNP_ID;
    messageToSend.args.bt_device_info_set_pnp_id.vendor_source_id = vendor_source_id;
    messageToSend.args.bt_device_info_set_pnp_id.vendor_id = vendor_id;
    messageToSend.args.bt_device_info_set_pnp_id.product_id = product_id;
    messageToSend.args.bt_device_info_set_pnp_id.product_version = product_version;

    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_set_pnp_id(
            messageToSend.args.bt_device_info_set_pnp_id.vendor_source_id,
            messageToSend.args.bt_device_info_set_pnp_id.vendor_id,
            messageToSend.args.bt_device_info_set_pnp_id.product_id,
            messageToSend.args.bt_device_info_set_pnp_id.product_version);
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_PNP_ID;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Gets the PnP ID on the GEM.
 * 
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_get_pnp_id(standard_response_t* p_response)
{
    

    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_PNP_ID;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_get_pnp_id();
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_PNP_ID;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Gets the BSCS Service Enabled State.
 * 
 * @param[out] p_response contains the enabled state for this service
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_get_bscs_service_enabled(standard_response_t* p_response)
{
    

    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_BSCS_SERVICE_ENABLED;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_get_bscs_service_included();
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_BSCS_SERVICE_ENABLED;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Sets the BSCS service enabled state
 *
 * @param[in] enabled 1 byte unsigned value denoting whether the state should e enabled (1) or disabled (0)
 * 
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_bscs_service_enabled(
    uint8_t enabled,
    standard_response_t* p_response)
{


    if(enabled != 1 &&
        enabled != 0)
    {
       return(NPE_GEM_RESPONSE_INVALID_PARAMETER); 
    }

    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BSCS_SERVICE_ENABLED;
    messageToSend.args.bt_device_info_bscs_service_set_enabled.enabled = enabled;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_set_bscs_service_included(messageToSend.args.bt_device_info_bscs_service_set_enabled.enabled);
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_BSCS_SERVICE_ENABLED;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Gets the POWER Service Enabled State.
 * 
 * @param[out] p_response contains the enabled state for this service
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_get_power_service_enabled(standard_response_t* p_response)
{
    

    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_POWER_SERVICE_ENABLED;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_get_power_service_included();
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_GET_POWER_SERVICE_ENABLED;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Sets the POWER service enabled state
 *
 * @param[in] enabled 1 byte unsigned value denoting whether the state should e enabled (1) or disabled (0)
 * 
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_power_service_enabled(
    uint8_t enabled,
    standard_response_t* p_response)
{


    if(enabled != 1 &&
        enabled != 0)
    {
       return(NPE_GEM_RESPONSE_INVALID_PARAMETER); 
    }

    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_POWER_SERVICE_ENABLED;
    messageToSend.args.bt_device_info_bscs_service_set_enabled.enabled = enabled;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_info_set_power_service_included(messageToSend.args.bt_device_info_power_service_set_enabled.enabled);
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_DEVICE_INFO_SET_POWER_SERVICE_ENABLED;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Starts bluetooth advertising on the GEM
 *
 * @param[out] p_advertising_start_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_control_start_advertising(standard_response_t* p_advertising_start_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONTROL;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONTROL_START_ADV;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_control_start_advertising();
 
    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONTROL;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONTROL_START_ADV;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_advertising_start_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Stops bluetooth advertising on the GEM
 *
 * @param[out] p_advertising_stop_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_control_stop_advertising(standard_response_t* p_advertising_stop_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONTROL;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONTROL_STOP_ADV;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_control_stop_advertising();

    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONTROL;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONTROL_STOP_ADV;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_advertising_stop_response->error_code = m_last_response.error_code;
    }
    return(res);
}




/** @brief Disconnects a Central Device from the GEM
 *
 * @param[out] p_disconnect_central_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_control_disconnect_central(standard_response_t* p_disconnect_central_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONTROL;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONTROL_DISCONNECT_CENTRAL;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_control_disconnect_central();

    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_CONTROL;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_BT_CONTROL_DISCONNECT_CENTRAL;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_disconnect_central_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Starts the discovery process for a bluetooth service.   
 *
 * @param[in] service_id    BT Service ID to start.
 * @param[in] minimum_rssi    RSSI filter for discovery.
 * @param[in] discovery_timeout    Discovery timeout in seconds. 0 disabled the timeout.
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_start_discovery(uint16_t bt_service_id, int8_t minimum_rssi, uint8_t discovery_timeout, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_START_DISCOVERY;
    messageToSend.args.bt_receiver_discovery.service_id = bt_service_id;
    messageToSend.args.bt_receiver_discovery.minimum_rssi = minimum_rssi;
    messageToSend.args.bt_receiver_discovery.discovery_timeout = discovery_timeout;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_receiver_start_discovery(
            messageToSend.args.bt_receiver_discovery.service_id,
            messageToSend.args.bt_receiver_discovery.minimum_rssi,
            messageToSend.args.bt_receiver_discovery.discovery_timeout);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}




/** @brief Stop the discovery process for bluetooth service. Set to 0 for iBoacon  
 *
 * @param[in] service_id    BT Service ID to stop. 0x0000 for iBeacon.
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_stop_discovery(uint16_t service_id, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_STOP_DISCOVERY;
    messageToSend.args.bt_receiver_stop_discovery.service_id = service_id;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_receiver_stop_discovery(messageToSend.args.bt_receiver_stop_discovery.service_id);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Connects to device with service id.
 *
 * @param[in] service_id            BT Service ID to connect to.
 * @param[in] p_bluetooth_address_base64_encoded   Pointer to 6 byte bluetooth address buffer (encoded as base64 string).
 * @param[in] length                length of the base64 encoded bluetooth address string/
 * @param[in] timeout               Connection timeout from 0 - 30s. 0 indicates 30s.
 * @param[in] use_gymconnect_events  If true use gymconnect class events instead of bluetooth receiver class..
 * 
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_connect_device_v2(uint16_t service_id, uint8_t* p_bluetooth_address_base64_encoded, int length, uint8_t connection_timeout, bool use_gymconnect_events, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();
    size_t len;
    uint8_t* bt_addr = base64_decode(p_bluetooth_address_base64_encoded, length,
			      &len);



    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_CONNECT_DEVICE;
    messageToSend.args.bt_receiver_connect_device.service_id = service_id;
    memcpy(messageToSend.args.bt_receiver_connect_device.bluetooth_address, bt_addr, BT_ADDRESS_SIZE);
    messageToSend.args.bt_receiver_connect_device.connection_timeout = connection_timeout;
    messageToSend.args.bt_receiver_connect_device.use_gymconnect_events = use_gymconnect_events;
    LOGE("BT ADDRESS CONNECT 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", 
        bt_addr[0],
        bt_addr[1],
        bt_addr[2],
        bt_addr[3],
        bt_addr[4],
        bt_addr[5]);
    

    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_receiver_connect_device(
            messageToSend.args.bt_receiver_connect_device.service_id,
            messageToSend.args.bt_receiver_connect_device.bluetooth_address,
            messageToSend.args.bt_receiver_connect_device.connection_timeout,
            messageToSend.args.bt_receiver_connect_device.use_gymconnect_events);

          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);

}

/** @brief Connects to device with service id. Use Bluetooth Receiver class events.
 *
 * @param[in] service_id            BT Service ID to connect to.
 * @param[in] p_bluetooth_address_base64_encoded   Pointer to 6 byte bluetooth address buffer (encoded as base64 string).
 * @param[in] length                length of the base64 encoded bleutooth address string/
 * @param[in] timeout               Connection timeout from 0 - 30s. 0 indicates 30s.
 * 
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_connect_device(uint16_t service_id, uint8_t* p_bluetooth_address_base64_encoded, int length, uint8_t connection_timeout, standard_response_t* p_response)
{
    return npe_hci_library_send_command_bt_receiver_connect_device_v2(service_id, p_bluetooth_address_base64_encoded, length, connection_timeout, false, p_response);
}

/** @brief Disconnects device with service id.
 *
 * @param[in] service_id            BT Service ID to connect to.
 * 
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_disconnect_device(uint16_t service_id, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_DISCONNECT_DEVICE;
    messageToSend.args.bt_receiver_disconnect_device.service_id = service_id;
    

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_receiver_disconnect_device(
            messageToSend.args.bt_receiver_disconnect_device.service_id);

          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Start the discovery process for bluetooth iBeacon  
 *
 * @param[in] company_id        2 byte unsigned value denoting BT SIG assigned company id of the iBeacon.
 * @param[in] p_uuid            Pointer to 16 byte array with uuid of the iBeacon.
 * @param[in] min_rssi          Miniimum RSSI required to pass on disciver iBeacon events
 * @param[in] discovery_timeout Discovery timeout in seconds.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_start_discovery_ibeacon(uint16_t company_id, uint8_t* p_uuid, int8_t min_rssi, uint8_t discovery_timeout, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_BT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_BT_RECEIVER_START_DISCOVERY_IBEACON;
    
    messageToSend.args.bt_receiver_start_discovery_ibeacon.company_id = company_id;
    memcpy(messageToSend.args.bt_receiver_start_discovery_ibeacon.uuid, p_uuid, MAX_STANDARD_LONG_UUID_SIZE);
    messageToSend.args.bt_receiver_start_discovery_ibeacon.min_rssi = min_rssi;
    messageToSend.args.bt_receiver_start_discovery_ibeacon.discovery_timeout = discovery_timeout;
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_bluetooth_receiver_start_discovery_ibeacon(messageToSend.args.bt_receiver_start_discovery_ibeacon.company_id, 
            messageToSend.args.bt_receiver_start_discovery_ibeacon.uuid, 
            messageToSend.args.bt_receiver_start_discovery_ibeacon.min_rssi,
            messageToSend.args.bt_receiver_start_discovery_ibeacon.discovery_timeout);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);


    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Get if specified tx profile is enabled. 
 *
 * @param[in] profile ANT Profile.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_control_get_ant_profile_enable(uint8_t profile, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONTROL;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_ENABLED;
    messageToSend.args.ant_control_ant_profile_enable.profile = profile;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_control_get_ant_profile_enable(messageToSend.args.ant_control_ant_profile_enable.profile);
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONTROL;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_ENABLED;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }  
    return(res);
}

/** @brief Enable or disable ANT+ tx Profile
 *
 * @param[in] profile ANT Profile.
 * @param[in] enable_or_disable Enables or disables the profile.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_control_set_ant_profile_enable(uint8_t profile, uint8_t enable_or_disable, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONTROL;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_ENABLED;
    messageToSend.args.ant_control_ant_profile_enable.profile = profile;
    messageToSend.args.ant_control_ant_profile_enable.enable_or_disable = enable_or_disable;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_control_set_ant_profile_enable(messageToSend.args.ant_control_ant_profile_enable.profile, messageToSend.args.ant_control_ant_profile_enable.enable_or_disable);
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONTROL;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_ENABLED;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }  
    return(res);
}


/** @brief Get the ANT profile frequency diversity settings.
 *
 * @param[in] profile ANT Profile.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_control_get_ant_profile_frequency_diversity(uint8_t profile, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONTROL;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_FREQ_DIV;
    messageToSend.args.ant_control_frequency_diversity_enable.profile = profile;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_control_get_ant_profile_frequency_diversity(messageToSend.args.ant_control_frequency_diversity_enable.profile);
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONTROL;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_GET_ANTP_PROFILE_FREQ_DIV;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }  
    return(res);
}


/** @brief Sets the ANT profile frequency diversity settings.
 *
 * @param[in] profile ANT Profile.
 * @param[in] mode Sync or Async
 * @param[in] channel_a_frequency_index Index of channel A
 * @param[in] channel_b_frequency_index Index of channel B
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_control_set_ant_profile_frequency_diversity(
    uint8_t profile, 
    uint8_t mode,
    uint8_t channel_a_frequency_index,
    uint8_t channel_b_frequency_index,
    standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONTROL;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_FREQ_DIV;
    messageToSend.args.ant_control_frequency_diversity_enable.profile = profile;
    messageToSend.args.ant_control_frequency_diversity_enable.mode = mode;
    messageToSend.args.ant_control_frequency_diversity_enable.channel_a_frequency_index = channel_a_frequency_index;
    messageToSend.args.ant_control_frequency_diversity_enable.channel_b_frequency_index = channel_b_frequency_index;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_control_set_ant_profile_frequency_diversity(
                messageToSend.args.ant_control_frequency_diversity_enable.profile,
                messageToSend.args.ant_control_frequency_diversity_enable.mode,
                messageToSend.args.ant_control_frequency_diversity_enable.channel_a_frequency_index,
                messageToSend.args.ant_control_frequency_diversity_enable.channel_b_frequency_index); 
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONTROL;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONTROL_SET_ANTP_PROFILE_FREQ_DIV;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }  
    return(res);
}


/** @brief Get the ANT hardware version  
 *
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_get_hardware_version(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_HW_VER;
     
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_config_get_hardware_version();
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_HW_VER;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Send the ANT hardware revision to GEM 
 *    
 * @param[in] hardware_version 1 byte unsigned value denoting ANT hardware revision.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_hardware_version(uint8_t hardware_version, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_HW_VER;
    messageToSend.args.ant_config_set_hardware_version.hardware_version = hardware_version;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_config_set_hardware_version(messageToSend.args.ant_config_set_hardware_version.hardware_version);
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_HW_VER;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Get the ANT manufacturer id 
 *
 * @param[out] p_response is the error code recieved from the GEM
 * 
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_get_manufacturer_id(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_MANU_ID;
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_config_get_manufacturer_id();
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_MANU_ID;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}


/** @brief Set the ANT manufacturer id 
 *
 * @param[in] manufacturer_id 2 byte unsigned value denoting ANT manufacturer id.
 * @param[out] p_response is the error code recieved from the GEM
 * 
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_manufacturer_id(uint16_t manufacturer_id, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MANU_ID;
    messageToSend.args.ant_config_manufacturer_id.manufacturer_id = manufacturer_id;
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_config_set_manufacturer_id(messageToSend.args.ant_config_manufacturer_id.manufacturer_id);
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MANU_ID;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}


/** @brief Send the ANT model number to GEM 
 *
 * @param[in] model_number 2 byte unsigned value denoting ANT model number.
 * @param[out] p_response is the error code recieved from the GEM
 * 
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_model_number(uint16_t model_number, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MODEL_NUM;
    messageToSend.args.ant_config_set_model_number.model_number = model_number;
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_config_set_model_number(messageToSend.args.ant_config_set_model_number.model_number);
  
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_MODEL_NUM;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Send the ANT software version to GEM 
 *
 * @param[in] main 1 byte unsigned value denoting the main software version.
 * @param[in] main 1 byte unsigned value denoting the supplemental software version.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_software_version(uint8_t main, uint8_t supplemental, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();
 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SW_VER;
    messageToSend.args.ant_config_set_version.main = main;
    messageToSend.args.ant_config_set_version.supplemental = supplemental;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_config_set_software_version(main, supplemental);
 
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SW_VER;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}


/** @brief Gets the ANT device number from GEM 
 *
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_get_ant_device_number(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_ANT_DEVICE_NUMBER;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_config_get_ant_device_number();
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_GET_ANT_DEVICE_NUMBER;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
       memcpy (p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Sets the ANT device number to GEM 
 *
 * @param[in] device_number_type is the device number type (0 = 16 bit, 1 = 20 bit)
 * @param[in] device_number is the ANT device number
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_ant_device_number(uint8_t device_number_type, uint32_t device_number, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_ANT_DEVICE_NUMBER;
    messageToSend.args.ant_config_set_device_number.device_number_type = device_number_type;
    messageToSend.args.ant_config_set_device_number.device_number = device_number;

    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_config_set_ant_device_number(device_number_type, device_number);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_ANT_DEVICE_NUMBER;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}


/** @brief Send the ANT serial number to GEM 
 *
 * @param[in] serial_number 4 byte unsigned value denoting ANT serial number.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_serial_number(uint32_t serial_number, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SERIAL_NUMBER;
    messageToSend.args.ant_config_set_serial_number.serial_number = serial_number;
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_config_set_serial_number(messageToSend.args.ant_config_set_serial_number.serial_number);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_CONFIG;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_CONFIG_SET_SERIAL_NUMBER;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Starts ANT+ discovery Process
 *
 * @param[in] ant_plus_profile Is the ANT+ profile to start discovering
 * @param[in] proximity_bin Proximity setting (1-10, 0 is disable)
 * @param[in] discovery_timeout Timeout in seconds - 0 is disable
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_start_discovery(uint16_t ant_plus_profile, uint8_t proximity_bin, uint8_t discovery_timeout, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    LOGI("Start ANT+ Discovery Profile %d", ant_plus_profile);

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_START_DISCOVERY;
    messageToSend.args.ant_receiver_start_discovery.ant_plus_device_profile = ant_plus_profile;
    messageToSend.args.ant_receiver_start_discovery.prximity_bin = proximity_bin;
    messageToSend.args.ant_receiver_start_discovery.discovery_timeout = discovery_timeout;
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_receiver_start_discovery(messageToSend.args.ant_receiver_start_discovery.ant_plus_device_profile, 
            messageToSend.args.ant_receiver_start_discovery.prximity_bin, 
            messageToSend.args.ant_receiver_start_discovery.discovery_timeout);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_START_DISCOVERY;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Stops ANT+ discovery Process
 *
 * @param[in] ant_plus_profile Is the ANT+ profile to start discovering
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_stop_discovery(uint16_t ant_plus_profile, standard_response_t* p_response)
{
     bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_STOP_DISCOVERY;
    messageToSend.args.ant_receiver_stop_discovery.ant_plus_device_profile = ant_plus_profile;
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_receiver_stop_discovery(messageToSend.args.ant_receiver_stop_discovery.ant_plus_device_profile);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_STOP_DISCOVERY;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Connect to specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[in] device_number Device Number of Devide to connect to or 0 for wildcard.
 * @param[in] proximity_bin Proximity bin (1-10 or 0 to disable).
 * @param[in] connection_timeout Connection timeout in seconds.
 * @param[out] p_advertising_start_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_connect_device(uint16_t ant_plus_device_profile, uint32_t device_number, 
    uint8_t proximity_bin, uint8_t connection_timeout, standard_response_t* p_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock();

    LOGI("ANT+ Connect Profile %d Dev# %d Prox Bin %d Timeout %d",
        ant_plus_device_profile,
        device_number,
        proximity_bin,
        connection_timeout);

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_CONNECT_DEVICE;
    messageToSend.args.ant_receiver_connect_device.ant_plus_device_profile = ant_plus_device_profile;
    messageToSend.args.ant_receiver_connect_device.device_number = device_number;
    messageToSend.args.ant_receiver_connect_device.proximity_bin = proximity_bin;
    messageToSend.args.ant_receiver_connect_device.connection_timeout = connection_timeout;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_receiver_connect_device(
            messageToSend.args.ant_receiver_connect_device.ant_plus_device_profile,
            messageToSend.args.ant_receiver_connect_device.device_number,
            messageToSend.args.ant_receiver_connect_device.proximity_bin,
            messageToSend.args.ant_receiver_connect_device.connection_timeout);

    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_CONNECT_DEVICE;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Disconnect from specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[out] p_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_disconnect_device(uint16_t ant_plus_device_profile, standard_response_t* p_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_DISCONNECT_DEVICE;
    messageToSend.args.ant_receiver_disconnect_device.ant_plus_device_profile = ant_plus_device_profile;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_receiver_disconnect_device(
            messageToSend.args.ant_receiver_disconnect_device.ant_plus_device_profile);

    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_DISCONNECT_DEVICE;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}


/** @brief Request Calibration from specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[in] attempts Number of times to try sending the request over the air.
 * @param[out] p_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_request_calibration(uint16_t ant_plus_device_profile, uint8_t attempts, standard_response_t* p_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_REQUEST_CALIBRATION;
    messageToSend.args.ant_receiver_request_calibration.ant_plus_device_profile = ant_plus_device_profile;
    messageToSend.args.ant_receiver_request_calibration.attempts = attempts;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_receiver_request_calibration(
            messageToSend.args.ant_receiver_request_calibration.ant_plus_device_profile,
            messageToSend.args.ant_receiver_request_calibration.attempts);

    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_REQUEST_CALIBRATION;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}


/** @brief Get Manufacturer Info from specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[out] p_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_get_manufacturer_info(uint16_t ant_plus_device_profile, standard_response_t* p_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_MANUFACTURER_INFO;
    messageToSend.args.ant_receiver_get_manufacturer_info.ant_plus_device_profile = ant_plus_device_profile;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_receiver_get_manufacturer_info(
            messageToSend.args.ant_receiver_get_manufacturer_info.ant_plus_device_profile);

    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_MANUFACTURER_INFO;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}


/** @brief Request Battery Status from specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[in] battery_index Battery Index - use 0xFF if unknown
 * @param[out] p_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_request_battery_status(uint16_t ant_plus_device_profile, uint8_t battery_index, standard_response_t* p_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_BATTERY_STATUS;
    messageToSend.args.ant_receiver_request_battery_status.ant_plus_device_profile = ant_plus_device_profile;
    messageToSend.args.ant_receiver_request_battery_status.data.battery_index = battery_index;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_ant_receiver_request_battery(
            messageToSend.args.ant_receiver_request_battery_status.ant_plus_device_profile,
            messageToSend.args.ant_receiver_request_battery_status.data.battery_index);

    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_ANT_RECEIVER_GET_BATTERY_STATUS;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }
    return(res);
}

/** @brief Enables the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_radio_enable(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_ENABLE_RADIO;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_enable_radio();
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_ENABLE_RADIO;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}


/** @brief Disables the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_radio_disable(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_DISABLE_RADIO;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_disable_radio();
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_DISABLE_RADIO;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Sets the scan period for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_set_scan_period(uint16_t scan_period_in_milliseconds, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SCAN_PERIOD;
    messageToSend.args.nfc_reader_set_scan_period.scan_period_in_milliseconds = scan_period_in_milliseconds;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_set_scan_period(scan_period_in_milliseconds);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SCAN_PERIOD;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Gets the scan period for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_get_scan_period(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SCAN_PERIOD;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_get_scan_period();
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SCAN_PERIOD;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
        p_response->args.nfc_reader_scan_period.nfc_reader_scan_period = m_last_response.args.nfc_reader_scan_period.nfc_reader_scan_period;
    }
    return(res);
}


/** @brief Sets the supported tag types for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_set_supported_tag_types(uint16_t tag_type_bitfield, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SUPPORTED_TAGS_TYPES;
    messageToSend.args.nfc_reader_set_tag_types.tag_types_bitfield = tag_type_bitfield;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_set_supported_tag_types(tag_type_bitfield);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_SUPPORTED_TAGS_TYPES;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Gets the supported tag types for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_get_supported_tag_types(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SUPPORTED_TAGS_TYPES;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_get_supported_tag_types();
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_GET_SUPPORTED_TAGS_TYPES;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
        p_response->args.nfc_reader_supported_tag_types.nfc_reader_supported_tag_types = m_last_response.args.nfc_reader_supported_tag_types.nfc_reader_supported_tag_types;
    }
    return(res);
}

/** @brief Sets the supported tag types for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_set_radio_test_mode(uint8_t mode, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_NFC_RADIO_TEST_MODE;
    messageToSend.args.nfc_reader_set_radio_test_mode.mode = mode;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_set_radio_test_mode(mode);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_READER_SET_NFC_RADIO_TEST_MODE;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Sets the tag board configuration  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_set_tag_board_configuration(const wf_gem_hci_nfc_tag_board_configuration_t* p_config, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    LOGI("Android NDEF %d %d %d %d", 
        p_config->android_ndef_payload_length, 
        p_config->android_ndef_payload_data[0],
        p_config->android_ndef_payload_data[1],
        p_config->android_ndef_payload_data[2]);
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_SET_NFC_TAG_CONFIGURATION;
    memcpy(&messageToSend.args.nfc_reader_set_tag_board_config.config, p_config, sizeof(wf_gem_hci_nfc_tag_board_configuration_t));

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_set_tag_board_configuration(p_config);
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_SET_NFC_TAG_CONFIGURATION;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Gets the NFC tag board configuration 
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_get_tag_board_configuration(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_GET_NFC_TAG_CONFIGURATION;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_get_tag_board_configuration();
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_GET_NFC_TAG_CONFIGURATION;
#endif



    if(res == NPE_GEM_RESPONSE_OK)
    {
        memcpy(p_response, &m_last_response,sizeof(standard_response_t));
    }

    return(res);
}

/** @brief NFC Radio Tag Comms Check
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_tag_board_comms_check(standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    // TODO - check args for limits!!

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_COMMUNICATION_CHECK;

    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_send_command_nfc_reader_tag_board_comms_check();
          
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_NFC_READER;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_NFC_TAG_BOARD_COMMUNICATION_CHECK;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}



/** @brief Set GEM controllable features
 *
 * @param[in] equipment_control_field_identifier 4 byte unsigned bitfield denoting denoting contollable feautures.
 * @param[out] p_response is the error code recieved from the GEM
 * 
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_gymconnect_set_supported_equipment_control_features(uint32_t equipment_control_field_identifier, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock(); 
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_GYM_CONNECT;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_CONTROL_FEATURES;
    messageToSend.args.gymconnect_set_supported_equipment_control_features.equipment_control_field_identifier = equipment_control_field_identifier;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else 
        wf_gem_hci_manager_gymconnect_set_supported_equipment_control_features(messageToSend.args.gymconnect_set_supported_equipment_control_features.equipment_control_field_identifier);
    
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_GYM_CONNECT;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_CONTROL_FEATURES;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Send the fitness equipment type to GEM 
 *
 * @param[in] fe_type is an enum denoting the fitness equipment type.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_gymconnect_set_fe_type(wf_gem_hci_gymconnect_fitness_equipment_type_e fe_type, standard_response_t* p_response)
{
    bool locked = npe_serial_transmit_lock();

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_GYM_CONNECT;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TYPE;
    messageToSend.args.gymconnect_set_fe_type.fe_type = fe_type;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_gymconnect_set_fe_type(messageToSend.args.gymconnect_set_fe_type.fe_type);
            
    uint32_t res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_GYM_CONNECT;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TYPE;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Send the fitness equipment state to GEM 
 *
 * @param[in] fe_state is an enum denoting the fitness equipment state.
 * @param[out] p_fe_state_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_gymconnect_set_fe_state(wf_gem_hci_gymconnect_fitness_equipment_state_e fe_state, standard_response_t* p_fe_state_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock();
    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_GYM_CONNECT;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_STATE;
    messageToSend.args.gymconnect_set_fe_state.fe_state = fe_state;
    
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_gymconnect_set_fe_state(messageToSend.args.gymconnect_set_fe_state.fe_state);
            
    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 10);

#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_GYM_CONNECT;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_STATE;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        p_fe_state_response->error_code = m_last_response.error_code;
    }
    return(res);
}
/** @brief Sends set fitness equipment data to the GEM
 *
 * @param[out] p_update_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_gymconnect_perform_workout_data_update(standard_response_t* p_update_response)
{
    uint32_t res;
    bool locked = npe_serial_transmit_lock(); // Check if on same thread - lock if not

    messageToSend.message_class_id = WF_GEM_HCI_MSG_CLASS_GYM_CONNECT;
    messageToSend.message_id = WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_UPDATE_WORKOUT_DATA;
    
    // If we are on the same thread, just send the message. 
    // Otherwise need to marshall to the tx thread. 
    if(locked)
        npe_serial_transmit_message_and_unlock();
    else
        wf_gem_hci_manager_gymconnect_perform_workout_data_update();
    
    res = npe_serial_interface_wait_for_response(npe_gem_library_check_if_response_received, 20);
    
#ifdef NO_GEM_BOARD_TEST
    receivedMessage.message_class_id = WF_GEM_HCI_MSG_CLASS_GYM_CONNECT;
    receivedMessage.message_id = WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_UPDATE_WORKOUT_DATA;
#endif

    if(res == NPE_GEM_RESPONSE_OK)
    {
        LOGI("Data update 0x%02x 0x%02x\n", receivedMessage.message_class_id, receivedMessage.message_id);  
        p_update_response->error_code = m_last_response.error_code;
    }
    return(res);
}

/** @brief Starts DFU process for device. Does NOT return until process is complete.
 *
 * @param[out] p_update_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 *          ::NPE_GEM_RESPONSE_SERIAL_OPEN_FAIL
 *          ::NPE_GEM_RESPONSE_UNABLE_TO_WRITE
 *          ::NPE_GEM_RESPONSE_UNKNOWN_ERROR
 * 
 */
uint32_t npe_hci_dfu_start_update(char* comport, char* zip_package_name, bool is_usb_transport,  dfu_progress_callback_t progress_cb, dfu_transfer_type_callback_t transfer_type_callback)
{
    uint32_t error_code = NPE_GEM_RESPONSE_OK;

    int ret = do_serial_dfu(comport, zip_package_name, is_usb_transport, (ProgressCallback)progress_cb, (TransferTypeCallback)transfer_type_callback);

    switch(ret)
    {
        case 0:
        {
            error_code = NPE_GEM_RESPONSE_OK;
            break;
        }
        case BAD_COM_PORT_OR_FILE:
        {
            error_code = NPE_GEM_RESPONSE_INVALID_PARAMETER;
            break;
        }
        case ERROR_OPENING_PORT:
        {
            error_code = NPE_GEM_RESPONSE_SERIAL_OPEN_FAIL;
            break;
        }
        case ERROR_SENDING_FILE:
        {
            error_code = NPE_GEM_RESPONSE_UNABLE_TO_WRITE;
            break;
        }
        default:
        {
            error_code = NPE_GEM_RESPONSE_UNKNOWN_ERROR;
            break;
        }
    }
    return error_code;

}


/******************** WF functions defined ************************************************************************************************/


void wf_gem_hci_comms_on_send_byte(uint8_t tx_byte)
{
    uint32_t err = npe_serial_interface_send_byte(tx_byte);
    assert(err == NPE_GEM_RESPONSE_OK);

}
// Called on RX thread.
void wf_gem_hci_comms_on_message_received(wf_gem_hci_comms_message_t* message)
{
    // Send to the HCI lib to process the received message
    wf_gem_hci_manager_process_recevied_message(message);

    // Signal other threads that the message has been processed. 
    npe_serial_interface_signal_response(npe_gem_hci_library_process_received_msg, (void*)message, sizeof(wf_gem_hci_comms_message_t));

}

void wf_gem_hci_manager_on_begin_retry_timer(uint16_t cmd_timeout_ms)
{
    //npe_serial_interface_start_retry_timer(cmd_timeout_ms);
}

// cancel/stop the timer started in wf_gem_hci_manager_on_begin_retry_timer()
void wf_gem_hci_manager_on_cancel_retry_timer(void)
{
    //npe_serial_interface_cancel_retry_timer();
}

// This callback is called if a command message can not be sent and all retries have failed.
void wf_gem_hci_manager_on_command_send_failure(wf_gem_hci_comms_message_t* message)
{
    LOGI("wf_gem_hci_manager_on_command_send_failure\n");
    fflush(stdout);
    assert(false);
}




// ****** Process Command Responses. *************************************************************** //
void _wf_gem_hci_manager_trigger_event(uint8_t message_class_id, uint8_t event_id, gem_event_args_t *p_data)
{
    gem_event_t event;
    event.message_class.type = message_class_id;
    event.event_id.id = event_id;
    if (p_data == NULL)
    {
        event.args.uint16_value = 0;
    }
    else
    {
        event.args = *p_data;
    }

    trigger_event(&event);
}

void wf_gem_hci_manager_on_command_response_generic(uint8_t error_code)
{
    m_last_response.error_code = error_code;
    LOGI("Error code:  %d\n",error_code);
}

void wf_gem_hci_manager_on_command_response_hardware_get_pin(uint8_t number_of_pins, uint8_t* p_pin_info)
{
    m_last_response.args.hw_pins.number_of_pins = number_of_pins;
    for(int i = 0; i < number_of_pins; i++){
        int j = i*2;
        m_last_response.args.hw_pins.pin_config[i].pin_number = p_pin_info[j];
        m_last_response.args.hw_pins.pin_config[i].pin_io_mode = p_pin_info[j+1];
    }
}
void wf_gem_hci_manager_on_command_response_bluetooth_control_start_advertising(uint8_t error_code)
{   
    m_last_response.error_code = error_code;
}

void wf_gem_hci_manager_on_command_response_bluetooth_control_stop_advertising(uint8_t error_code)
{
    
}

void wf_gem_hci_manager_on_command_response_bluetooth_config_set_device_name(uint8_t error_code)
{
    m_last_response.error_code = error_code;
}

void wf_gem_hci_manager_on_command_response_bluetooth_info_get_pnp_id(uint8_t vendor_source_id, uint16_t vendor_id, uint16_t product_id, uint16_t product_version)
{
    m_last_response.args.bt_device_info_get_pnp_id.vendor_source_id = vendor_source_id;
    m_last_response.args.bt_device_info_get_pnp_id.vendor_id = vendor_id;
    m_last_response.args.bt_device_info_get_pnp_id.product_id = product_id;
    m_last_response.args.bt_device_info_get_pnp_id.product_version = product_version;
}


void wf_gem_hci_manager_on_command_response_bluetooth_info_get_bscs_service_enabled(uint8_t enabled)
{
    m_last_response.args.bt_device_info_bsbc_service_get_enabled.enabled = enabled;
}

void wf_gem_hci_manager_on_command_response_bluetooth_info_get_power_service_enabled(uint8_t enabled)
{
    m_last_response.args.bt_device_info_power_service_get_enabled.enabled = enabled;
}



void wf_gem_hci_manager_on_command_response_system_ping(void)
{
    LOGI("wf_gem_hci_manager_on_command_response_system_ping\n");
}

void wf_gem_hci_manager_on_command_response_system_shutdown(uint8_t error_code)
{
    LOGI("wf_gem_hci_manager_on_command_response_system_shutdown\n");
}



void wf_gem_hci_manager_on_command_response_system_get_gem_module_version_info(wf_gem_hci_system_gem_module_version_info_t *version_info)
{
    m_last_response.error_code = 0;
    memcpy(&m_last_response.args.system_version.gem_version, version_info, sizeof(wf_gem_hci_system_gem_module_version_info_t));

    LOGI("Got version in C code %d %d %d %d %d %d %d %d %d %d %d\n", 
        m_last_response.args.system_version.gem_version.vendor_id, 
        m_last_response.args.system_version.gem_version.product_id, 
        m_last_response.args.system_version.gem_version.hw_version,
        m_last_response.args.system_version.gem_version.fw_version_major, 
        m_last_response.args.system_version.gem_version.fw_version_minor, 
        m_last_response.args.system_version.gem_version.fw_version_build,
        m_last_response.args.system_version.gem_version.fw_version_simple, 
        m_last_response.args.system_version.gem_version.bl_version_major, 
        m_last_response.args.system_version.gem_version.bl_version_vendor,
        m_last_response.args.system_version.gem_version.bl_version_device_variant, 
        m_last_response.args.system_version.gem_version.bl_version_revision);
}


void wf_gem_hci_manager_on_command_response_system_reset(uint8_t error_code)
{
    LOGI("wf_gem_hci_manager_on_command_response_system_reset\n");
}

void wf_gem_hci_manager_on_event_system_powerup(void)
{
    LOGI("wf_gem_hci_manager_on_event_system_powerup\n");

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_SYSTEM,
                 WF_GEM_HCI_EVENT_ID_SYSTEM_POWER_UP,
                 NULL);
}

void wf_gem_hci_manager_on_event_system_bootloader_initiated(uint8_t interface)
{
    LOGI("wf_gem_hci_manager_on_event_system_bootloader_initiated\n");
    gem_event_args_t args = {
        .system_bootloader_initiated.interface = interface
    };   
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_SYSTEM,
                 WF_GEM_HCI_EVENT_ID_SYSTEM_BOOTLOADER_INITIATED,
                 &args);
}


void wf_gem_hci_manager_on_event_system_shutdown(void)
{
    LOGI("wf_gem_hci_manager_on_event_system_shutdown\n");

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_SYSTEM,
                  WF_GEM_HCI_EVENT_ID_SYSTEM_SHUTDOWN,
                  NULL);
}

// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// 		Bluetooth Control Command Responses, Events
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_on_command_response_bluetooth_control_get_bluetooth_state(wf_gem_hci_bluetooth_state_e bluetooth_state)
{
    LOGI("wf_gem_hci_manager_on_command_response_bluetooth_control_get_bluetooth_state\n");
}

void wf_gem_hci_manager_on_event_bluetooth_control_advertising_timed_out(void)
{
    LOGI("wf_gem_hci_manager_on_event_bluetooth_control_advertising_timed_out\n");
    fflush(stdout);

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_CONTROL,
                  WF_GEM_HCI_EVENT_ID_BT_CONTROL_ADV_TIMEOUT,
                  NULL);
}

void wf_gem_hci_manager_on_event_bluetooth_control_connected(uint16_t connection_handle, uint8_t peer_address_type, uint8_t* p_peer_address, uint8_t device_type)
{
    LOGI("wf_gem_hci_manager_on_event_bluetooth_control_connected\n");

    gem_event_args_t args;
    args.bt_control_connected.connection_handle  = connection_handle;
    args.bt_control_connected.peer_address_type = peer_address_type;
    memcpy(args.bt_control_connected.peer_address, p_peer_address, BT_ADDRESS_SIZE);
    args.bt_control_connected.device_type = device_type;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_CONTROL,
                  WF_GEM_HCI_EVENT_ID_BT_CONTROL_CENTRAL_CONNECTED,
                  &args);
}


void wf_gem_hci_manager_on_event_bluetooth_control_connected_v1(void)
{
    LOGI("wf_gem_hci_manager_on_event_bluetooth_control_connected_v1\n");

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_CONTROL,
                  WF_GEM_HCI_EVENT_ID_BT_CONTROL_CENTRAL_CONNECTED_V1,
                  NULL);
}
void wf_gem_hci_manager_on_event_bluetooth_control_disconnected(bool peripheral_solicited, bool central_solicited)
{
    LOGI("wf_gem_hci_manager_on_event_bluetooth_control_disconnected\n");

    gem_event_args_t args;
    args.bt_control_disconnected.peripheral_solicited = peripheral_solicited;
    args.bt_control_disconnected.central_solicited = central_solicited;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_CONTROL,
                  WF_GEM_HCI_EVENT_ID_BT_CONTROL_CENTRAL_DISCONNECTED,
                  &args);
}



// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// Bluetooth Configuration Command Responses
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_on_command_response_bluetooth_config_get_device_name(utf8_data_t* device_name)
{
    LOGI("wf_gem_hci_manager_on_command_response_bluetooth_config_get_device_name\n");
}

void wf_gem_hci_manager_on_command_response_bluetooth_config_get_advertising_intervals_and_timeouts(
    uint16_t fast_advertising_interval, uint16_t fast_advertising_timeout, 
    uint16_t normal_advertising_interval, uint16_t normal_advertising_timeout)
{
    m_last_response.error_code = 0;
    m_last_response.args.bt_config_set_adv_interval_and_timeouts.fast_advertising_interval = fast_advertising_interval;
    m_last_response.args.bt_config_set_adv_interval_and_timeouts.fast_advertising_timeout = fast_advertising_timeout;
    m_last_response.args.bt_config_set_adv_interval_and_timeouts.normal_advertising_interval = normal_advertising_interval;
    m_last_response.args.bt_config_set_adv_interval_and_timeouts.normal_advertising_timeout = normal_advertising_timeout;
}


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// Bluetooth Receiver Events and Responses
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

void wf_gem_hci_manager_on_command_response_bt_response_start_discovery(uint16_t service_id, uint8_t err_code)
{
    m_last_response.error_code = err_code;
    m_last_response.args.bt_receiver_start_discovery_resp.service_id = service_id;
   
    LOGI("Start Discovery Response. Err Code: %d Service ID %d\n",err_code, service_id);
}

void wf_gem_hci_manager_on_command_response_bt_response_stop_discovery(uint16_t service_id, uint8_t err_code)
{
    m_last_response.error_code = err_code;
    m_last_response.args.bt_receiver_stop_discovery.service_id = service_id;
   
    LOGI("Stop Discovery Response. Err Code: %d Service ID %d\n",err_code, service_id);
}

void wf_gem_hci_manager_on_command_response_bt_response_connect_device(uint16_t service_id, uint8_t err_code)
{
    m_last_response.error_code = err_code;
    m_last_response.args.bt_receiver_connect_device_resp.service_id = service_id;
   
    LOGI("Connect Device Response. Err Code: %d Service ID %d\n",err_code, service_id);
}

void wf_gem_hci_manager_on_command_response_bt_response_disconnect_device(uint16_t service_id, uint8_t err_code)
{
    m_last_response.error_code = err_code;
    m_last_response.args.bt_receiver_disconnect_device_resp.service_id = service_id;
   
    LOGI("Disconnect Device Response. Err Code: %d Service ID %d\n",err_code, service_id);
}



void wf_gem_hci_manager_on_event_bt_receiver_discovery_timeout(uint16_t service_id)
{
    LOGI("Discovery Timeout. Service ID 0x%04x\n", service_id);

    gem_event_args_t args;
    args.bt_receiver_discovery_timeout.service_id = service_id;
    
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DISCOVERY_TIMEOUT,
                  &args);
}

void wf_gem_hci_manager_on_event_bt_receiver_device_connected(uint16_t service_id, const uint8_t* bt_address)
{
    LOGI("Device Connected. Service ID 0x%04x\n", service_id);

    gem_event_args_t args;
    args.bt_receiver_device_connected.service_id = service_id;
    memcpy(args.bt_receiver_device_connected.bt_address, bt_address, BT_ADDRESS_SIZE);

    
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DEVICE_CONNECTED,
                  &args);
}


void wf_gem_hci_manager_on_event_bt_receiver_device_connection_failure(uint16_t service_id, uint8_t failure_reason)
{
    LOGI("Device Connection Failed. Service ID 0x%04x Reason: %d\n", service_id, failure_reason);

    gem_event_args_t args;
    args.bt_receiver_device_connection_failed.service_id = service_id;
    args.bt_receiver_device_connection_failed.failure_reason = failure_reason;

    
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECT_FAIL,
                  &args);
}

void wf_gem_hci_manager_on_event_bt_receiver_device_disconnected(uint16_t service_id, uint8_t disconnect_reason)
{
    LOGI("Device Disconnected. Service ID 0x%04x Reason: %d\n", service_id, disconnect_reason);

    gem_event_args_t args;
    args.bt_receiver_device_disconnected.service_id = service_id;
    args.bt_receiver_device_disconnected.disconnect_reason = disconnect_reason;

    
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DEVICE_DISCONNECTED,
                  &args);
}

void wf_gem_hci_manager_on_event_bt_receiver_device_discovered(uint16_t service_id, const uint8_t* bt_address, uint8_t local_name_type, uint8_t local_name_size, const char* local_name, int8_t rssi)
{
    LOGI("Device Discovered. Service ID 0x%04x\n", service_id);

    gem_event_args_t args;
    args.bt_receiver_device_discovered.service_id = service_id;
    memcpy(args.bt_receiver_device_discovered.bt_address, bt_address, BT_ADDRESS_SIZE);
    args.bt_receiver_device_discovered.local_name_type = local_name_type;

    memset(args.bt_receiver_device_discovered.local_name, 0, BT_MAX_LOCAL_NAME);
    if(local_name_size <= BT_MAX_LOCAL_NAME)
    { 
        memcpy(args.bt_receiver_device_discovered.local_name, local_name, local_name_size);
    }
    args.bt_receiver_device_discovered.rssi = rssi;
    
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DEVICE_DISCOVERED,
                  &args);
}



void wf_gem_hci_manager_on_event_bt_receiver_ibeacon_discovered(wf_gem_hci_bt_receiver_ibeacon_discovered_event_t* p_ibeacon_evt)
{
    LOGI("iBeacon Discovered. Company ID %d, RSSI %d\n", p_ibeacon_evt->company_id, p_ibeacon_evt->received_rssi);

    gem_event_args_t args;
    memcpy(&args.bt_receiver_ibeacon_discovered.beacon_info, p_ibeacon_evt, sizeof(wf_gem_hci_bt_receiver_ibeacon_discovered_event_t));

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_BT_RECEIVER_IBEACON_DISCOVERED,
                  &args);
}

void wf_gem_hci_manager_on_event_bt_receiver_heart_rate_data(wf_gem_hci_bt_receiver_heart_rate_data_event_t* p_hr_evt)
{
    LOGI("Bluetooth Heat Rate Receiver HR: %d\n", p_hr_evt->hr);

    gem_event_args_t args;
    memcpy(&args.bt_receiver_heart_rate_event, p_hr_evt, sizeof(wf_gem_hci_bt_receiver_heart_rate_data_event_t));

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_BT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECTED_HEART_RATE_MONITOR_DATA,
                  &args);
}



void wf_gem_hci_manager_gymconnect_on_command_send_failure(wf_gem_hci_comms_message_t *message)
{
    LOGI("wf_gem_hci_manager_gymconnect_on_command_send_failure\n");
}

void wf_gem_hci_manager_gymconnect_on_command_response_get_fe_type(wf_gem_hci_gymconnect_fitness_equipment_type_e fe_type)
{
    LOGI("wf_gem_hci_manager_gymconnect_on_command_response_get_fe_type\n");
}
void wf_gem_hci_manager_gymconnect_on_command_response_set_fe_type(uint8_t error_code)
{
    LOGI("wf_gem_hci_manager_gymconnect_on_command_response_set_fe_type\n");
}

void wf_gem_hci_manager_gymconnect_on_command_response_get_fe_state(wf_gem_hci_gymconnect_fitness_equipment_state_e fe_state)
{
    LOGI("wf_gem_hci_manager_gymconnect_on_command_response_get_fe_state\n");
}
void wf_gem_hci_manager_gymconnect_on_command_response_set_fe_state(uint8_t error_code)
{
    LOGI("wf_gem_hci_manager_gymconnect_on_command_response_set_fe_state\n");
}

void wf_gem_hci_manager_gymconnect_on_command_response_get_fe_program_name(utf8_data_t *program_name)
{
    LOGI("wf_gem_hci_manager_gymconnect_on_command_response_get_fe_program_name\n");
}
void wf_gem_hci_manager_gymconnect_on_command_response_set_fe_program_name(uint8_t error_code)
{
    LOGI("wf_gem_hci_manager_gymconnect_on_command_response_set_fe_program_name\n");
}

void wf_gem_hci_manager_gymconnect_on_workout_data_update_complete(uint8_t error_code)
{
    //LOGI("wf_gem_hci_manager_gymconnect_on_workout_data_update_complete\n");
}

void wf_gem_hci_manager_gymconnecton_event_heart_rate_value_received(uint16_t heart_rate_value, uint16_t avg_heart_rate_value, uint8_t source)
{
    LOGI("wf_gem_hci_manager_gymconnecton_event_heart_rate_value_received\n");

    gem_event_args_t args;
    args.gymconnect_event_heart_rate_value.heart_rate = heart_rate_value;
    args.gymconnect_event_heart_rate_value.avg_heart_rate = avg_heart_rate_value;
    args.gymconnect_event_heart_rate_value.source = source;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT,
                  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_HEART_RATE_VALUE_RECEIVED,
                  &args);

}
extern void wf_gem_hci_manager_gymconnecton_event_calorie_value_received(uint16_t accum_total_calories, uint16_t accum_average_calories, uint16_t current_calorie_rate, uint8_t source)
{
    LOGI("wf_gem_hci_manager_gymconnecton_event_calorie_value_received\n");

    gem_event_args_t args;
    args.gymconnect_event_calories_value.accumulated_total_calories = accum_total_calories;
    args.gymconnect_event_calories_value.accumulated_active_calories = accum_average_calories;
    args.gymconnect_event_calories_value.current_calorie_rate = current_calorie_rate;
    args.gymconnect_event_calories_value.source = source;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT,
                  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CALORIE_DATA_RECEIVED,
                  &args);
}


void wf_gem_hci_manager_gymconnecton_event_cadence_value_received(uint16_t cadence_value)
{
    LOGI("wf_gem_hci_manager_gymconnecton_event_cadence_value_received\n");

    gem_event_args_t args;
    args.uint16_value = cadence_value;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_GYM_CONNECT,
                  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CADENCE_VALUE_RECEIVED,
                  &args);
}


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
// ANT Reciever Response and Event handlers.
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+


void wf_gem_hci_manager_on_command_response_ant_receiver_start_discovery(uint16_t profile, uint8_t error_code)
{
    LOGI("ANT Discovery Start for profile %d has started.", profile);
    m_last_response.error_code = error_code;
    m_last_response.args.ant_receiver_start_discovery.ant_plus_device_profile = profile;
}

void wf_gem_hci_manager_on_command_response_ant_receiver_stop_discovery(uint16_t profile, uint8_t error_code)
{
    LOGI("ANT Discovery Stop for profile %d.", profile);
    m_last_response.error_code = error_code;
    m_last_response.args.ant_receiver_stop_discovery.ant_plus_device_profile = profile;
}


void wf_gem_hci_manager_on_command_response_ant_receiver_connect_device(uint16_t profile, uint8_t error_code)
{
    LOGI("ANT Connect Device profile %d.", profile);
    m_last_response.error_code = error_code;
    m_last_response.args.ant_receiver_connect_device.ant_plus_device_profile = profile;
}


void wf_gem_hci_manager_on_command_response_ant_receiver_disconnect_device(uint16_t profile, uint8_t error_code)
{
    LOGI("ANT Connect Device profile %d.", profile);
    m_last_response.error_code = error_code;
    m_last_response.args.ant_receiver_disconnect_device.ant_plus_device_profile = profile;
}

void wf_gem_hci_manager_on_command_response_ant_receiver_request_calibration(uint16_t profile, uint8_t error_code)
{
    LOGI("ANT Connect Device profile %d.", profile);
    m_last_response.error_code = error_code;
    m_last_response.args.ant_receiver_request_calibration.ant_plus_device_profile = profile;  
}

void wf_gem_hci_manager_on_command_response_ant_receiver_get_manufacturer_info(uint16_t profile, uint8_t error_code, const wf_gem_hci_ant_receiver_get_manufacturer_info_t* p_data)
{
    LOGI("ANT Connect Device profile %d.", profile);
    m_last_response.error_code = error_code;
    m_last_response.args.ant_receiver_get_manufacturer_info.ant_plus_device_profile = profile;  
    memcpy(&m_last_response.args.ant_receiver_get_manufacturer_info.data, p_data, sizeof(wf_gem_hci_ant_receiver_get_manufacturer_info_t));
}


void wf_gem_hci_manager_on_command_response_ant_receiver_request_battery_status(uint16_t profile, uint8_t error_code, const wf_gem_hci_ant_receiver_battery_status_data_t* p_data)
{
    LOGI("ANT Connect Device profile %d.", profile);
    m_last_response.error_code = error_code;
    m_last_response.args.ant_receiver_request_battery_status.ant_plus_device_profile = profile;  
    memcpy(&m_last_response.args.ant_receiver_request_battery_status.data, p_data, sizeof(wf_gem_hci_ant_receiver_battery_status_data_t));
}

void wf_gem_hci_manager_on_event_ant_receiver_discovery_timeout(uint16_t profile_id)
{
    LOGI("ANT Discovery Timeout. Service ID 0x%04x\n", profile_id);

    gem_event_args_t args;
    args.ant_receiver_discovery_timeout.ant_plus_profile_id = profile_id;
    
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DISCOVERY_TIMEOUT,
                  &args);
}

void wf_gem_hci_manager_on_event_ant_receiver_device_connected(uint16_t ant_plus_profile_id, uint32_t device_number)
{
    LOGI("ANT+ Device Connected. Profile ID %d, Device Number %d\n", ant_plus_profile_id, device_number);

    gem_event_args_t args;
    args.ant_receiver_device_connected.ant_plus_profile_id = ant_plus_profile_id;
    args.ant_receiver_device_connected.device_number = device_number;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_CONNECTED,
                  &args);
}

void wf_gem_hci_manager_on_event_ant_receiver_connected_device_timeout(uint16_t ant_plus_profile_id)
{
    LOGI("ANT+ Device Connected Timeout. Profile ID %d\n", ant_plus_profile_id);

    gem_event_args_t args;
    args.ant_receiver_device_connected_device_timeout.ant_plus_profile_id = ant_plus_profile_id;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECT_DEVICE_TIMEOUT,
                  &args);
}


void wf_gem_hci_manager_on_event_ant_receiver_device_disconnected(uint16_t ant_plus_profile_id, uint8_t disconnect_reason)
{
    LOGI("ANT+ Device Disconnected. Profile ID %d\n", ant_plus_profile_id);

    gem_event_args_t args;
    args.ant_receiver_device_disconnected.ant_plus_profile_id = ant_plus_profile_id;
    args.ant_receiver_device_disconnected.disconnect_reason = disconnect_reason;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_DISCONNECTED,
                  &args);
}




void wf_gem_hci_manager_on_event_ant_receiver_device_discovered(uint16_t ant_plus_profile_id, uint32_t device_number, int8_t rssi)
{
    LOGI("ANT+ Device Discovered. Profile ID %d, Device Number %d, RSSI %d\n", ant_plus_profile_id, device_number, rssi);

    gem_event_args_t args;
    args.ant_receiver_device_discovered.ant_plus_profile_id = ant_plus_profile_id;
    args.ant_receiver_device_discovered.device_number = device_number;
    args.ant_receiver_device_discovered.rssi = rssi;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_DISCOVERED,
                  &args);
}



void wf_gem_hci_manager_on_event_ant_receiver_heart_rate_data(uint8_t heart_rate)
{
    LOGI("ANT+ Heart Rate Data: %d\n", heart_rate);

    gem_event_args_t args;
    args.ant_receiver_heart_rate_data.heart_rate = heart_rate;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_HEART_RATE_MONITOR_DATA,
                  &args);
}

void wf_gem_hci_manager_on_event_ant_receiver_power_data(const wf_gem_hci_ant_receiver_power_data_event_t* p_power)
{
    LOGI("ANT+ Power: %d Cadence: %d\n", p_power->power, p_power->cadence);

    gem_event_args_t args;
    memcpy(&args.ant_receiver_power_data.data, p_power, sizeof(wf_gem_hci_ant_receiver_power_data_event_t));

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_BIKE_POWER_DATA,
                  &args);
}

void wf_gem_hci_manager_on_event_ant_receiver_power_calibration_response(uint8_t calibration_response, uint8_t auto_zero_support, uint8_t auto_zero_enabled, uint16_t calibration_data)
{
    LOGI("ANT+ Power Calibration Response: %d\n", calibration_response);

    gem_event_args_t args;

    args.ant_receiver_power_calibration_response.calibration_response = calibration_response;
    args.ant_receiver_power_calibration_response.auto_zero_support = auto_zero_support;
    args.ant_receiver_power_calibration_response.auto_zero_enabled = auto_zero_enabled;
    args.ant_receiver_power_calibration_response.calibration_data = calibration_data;

    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER,
                  WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_BIKE_POWER_CALIBRATION_RESPONSE,
                  &args);
}


// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
//		NFC reader and tag board events
// +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
void wf_gem_hci_manager_on_command_response_nfc_reader_enable_radio(uint8_t error_code) {
    LOGI("Enable NFC Reader response received.");
    m_last_response.error_code = error_code;
}

void wf_gem_hci_manager_on_response_nfc_reader_get_scan_period(uint16_t scan_period)
{
    LOGI("Get Scan Period %d", scan_period);
    m_last_response.args.nfc_reader_scan_period.nfc_reader_scan_period = scan_period;
}

void wf_gem_hci_manager_on_response_nfc_reader_supported_tag_types(uint16_t supported_tag_types)
{
    LOGI("Get Tag Types %d", supported_tag_types);
    m_last_response.args.nfc_reader_supported_tag_types.nfc_reader_supported_tag_types = supported_tag_types;
}
void wf_gem_hci_manager_on_response_nfc_tag_board_get_configuration(const wf_gem_hci_nfc_tag_board_configuration_t* p_config)
{
    LOGI("Get Tag Board Config");
    memcpy(&m_last_response.args.nfc_reader_set_tag_board_config.config, p_config, sizeof(wf_gem_hci_nfc_tag_board_configuration_t));
}

void wf_gem_hci_manager_on_event_nfc_reader_read(uint8_t nfc_tag_type, uint8_t nfc_tag_data_type, uint8_t* p_payload, uint8_t nfc_data_payload_length)
{
    gem_event_args_t args;
    args.nfc_reader_event_read.nfc_tag_type = nfc_tag_type;
    args.nfc_reader_event_read.nfc_tag_data_type = nfc_tag_data_type;
    args.nfc_reader_event_read.payload_length = nfc_data_payload_length;
    memcpy(args.nfc_reader_event_read.payload, p_payload, nfc_data_payload_length);
    
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                  WF_GEM_HCI_EVENT_ID_NFC_READER_READ_EVENT,
                  &args);
}

void wf_gem_hci_manager_on_event_nfc_ndef_read(uint8_t tnf_type, uint8_t record_type, uint16_t record_payload_length, uint8_t* p_payload)
{
    gem_event_args_t args;
    args.nfc_reader_event_NDEF_read.TNF_type = tnf_type;
    args.nfc_reader_event_NDEF_read.record_type = record_type;
    args.nfc_reader_event_NDEF_read.record_payload_length = record_payload_length;
    memcpy(args.nfc_reader_event_NDEF_read.payload, p_payload, record_payload_length);
    
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                  WF_GEM_HCI_EVENT_ID_NFC_NDEF_READ_EVENT,
                  &args);
}



void wf_gem_hci_manager_on_event_nfc_tag_board_read(void)
{
    TRIGGER_EVENT(WF_GEM_HCI_MSG_CLASS_NFC_READER,
                  WF_GEM_HCI_EVENT_ID_NFC_TAG_BOARD_READ_EVENT,
                  NULL);
}

void wf_gem_hci_manager_on_command_response_ant_config_get_ant_device_number(uint8_t device_number_type, uint32_t device_number)
{
    m_last_response.error_code = 0;
    m_last_response.args.ant_config_set_device_number.device_number_type = device_number_type;
    m_last_response.args.ant_config_set_device_number.device_number = device_number;
}

void wf_gem_hci_manager_on_command_response_ant_config_get_hardware_version(uint8_t hardware_version)
{
    m_last_response.error_code = 0;
    m_last_response.args.ant_config_set_hardware_version.hardware_version = hardware_version;
}

void wf_gem_hci_manager_on_command_response_ant_config_get_manufacturer_id(uint16_t manufacturer_id)
{
    m_last_response.error_code = 0;
    m_last_response.args.ant_config_manufacturer_id.manufacturer_id = manufacturer_id;
}

void wf_gem_hci_manager_on_command_response_ant_control_get_ant_profile_enable(uint8_t profile, uint8_t enabled_or_disabled)
{
    m_last_response.error_code = 0;
    m_last_response.args.ant_control_ant_profile_enable.profile = profile;
    m_last_response.args.ant_control_ant_profile_enable.enable_or_disable = enabled_or_disabled;
}

void wf_gem_hci_manager_on_command_response_ant_control_get_ant_profile_frequency_diversity_enable(
    uint8_t response, 
    uint8_t profile,
    uint8_t mode,
    uint8_t channel_a_frequency_index,
    uint8_t channel_b_frequency_index)
{
    m_last_response.error_code = response;
    m_last_response.args.ant_control_frequency_diversity_enable.profile = profile;
    m_last_response.args.ant_control_frequency_diversity_enable.mode = mode;
    m_last_response.args.ant_control_frequency_diversity_enable.channel_a_frequency_index = channel_a_frequency_index;
    m_last_response.args.ant_control_frequency_diversity_enable.channel_b_frequency_index = channel_b_frequency_index;
}

