#
# 'make depend' uses makedepend to automatically generate dependencies 
#               (dependencies are added to end of Makefile)
# 'make'        build executable file 'mycc'
# 'make clean'  removes all .o and executable files
#
HCI_LIB_DIR := ./hci_reference_lib
DFU_LIB_DIR := ./serial_dfu_host/UartSecureDFU

# define the C compiler to use
CC = gcc

# define any compile-time flags
CFLAGS = -pthread -Wall -g -std=c99

# define any directories containing header files other than /usr/include
#
INCLUDES = -I./hci_reference_lib -I./

# define library paths in addition to /usr/lib
#   if I wanted to include libraries not in /usr/lib I'd specify
#   their path using -Lpath, something like:
LFLAGS = -L../libs

# define any libraries to link into executable:
#   if I want to link in libraries (libx.so or libx.a) I use the -llibname 
#   option, something like (this will link in libmylib.so and libm.so:
LIBS = -lm -lserialport

# define the C source files
SRCS = \
	main.c \
	npe_gem_hci_serial_interface.c \
	npe_gem_hci_library_interface.c \
	base64.c \
	$(HCI_LIB_DIR)/wf_gem_hci_comms.c \
	$(HCI_LIB_DIR)/wf_gem_hci_manager.c \
	$(HCI_LIB_DIR)/wf_gem_hci_manager_bootloader.c \
	$(HCI_LIB_DIR)/wf_gem_hci_manager_gymconnect.c \
	$(DFU_LIB_DIR)/crc32.c \
	$(DFU_LIB_DIR)/delay_connect.c \
	$(DFU_LIB_DIR)/dfu_serial.c \
	$(DFU_LIB_DIR)/dfu.c \
	$(DFU_LIB_DIR)/jsmn.c \
	$(DFU_LIB_DIR)/logging.c \
	$(DFU_LIB_DIR)/slip_enc.c \
	$(DFU_LIB_DIR)/uart_linux.c \
	$(DFU_LIB_DIR)/uart_slip.c \
	$(DFU_LIB_DIR)/zip.c \
	$(DFU_LIB_DIR)/UartSecureDFU.c

# define the C object files 
#
# This uses Suffix Replacement within a macro:
#   $(name:string1=string2)
#         For each word in 'name' replace 'string1' with 'string2'
# Below we are replacing the sxxuffix .c of all words in the macro SRCS
# with the .o suffix
#
OBJS = $(SRCS:.c=.o)

# define the executable file 
MAIN = mycc

#
# The following part of the makefile is generic; it can be used to 
# build any executable just by changing the definitions above and by
# deleting dependencies appended to the file from 'make depend'
#

.PHONY: depend clean

all:    $(MAIN)
	@echo  Simple compiler named mycc has been compiled

$(MAIN): $(OBJS) 
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

# this is a suffix replacement rule for building .o's from .c's
# it uses automatic variables $<: the name of the prerequisite of
# the rule(a .c file) and $@: the name of the target of the rule (a .o file) 
# (see the gnu make manual section about automatic variables)
.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

clean:
	$(RM) *.o *~ $(MAIN)

depend: $(SRCS)
	makedepend $(INCLUDES) $^

# DO NOT DELETE THIS LINE -- make depend needs it