//  Copyright (c) 2017-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.

#include <stdio.h> 
#include <stdlib.h> 
#include <stdint.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdbool.h> 
#include <sys/types.h>
#include "wf_gem_hci_manager.h"
#include "npe_error_code.h"
#include "npe_gem_hci_library_interface.h"
#include "npe_gem_hci_library_response_strings.h"


typedef enum 
{
    NORMAL,
    BOOTLOADER
} main_state_t;


/** @brief Treadmill data struct.  */
typedef struct 
{
    uint16_t workout_time_in_seconds;
    uint16_t speed_centi_kph;
    uint16_t deci_grade;
    uint16_t kcal_total;
    uint16_t rate_kcal_per_hour;
    uint32_t distance_meters;
    uint16_t pace;
    uint32_t average_pace;
} treadmill_workout_data_t;

/** @brief Local variables.  */
char* mp_port = "COM6";
char* mp_dfu_filename = NULL;
static treadmill_workout_data_t m_workout_data;
static wf_gem_hci_gymconnect_fitness_equipment_state_e m_equipment_state = WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IDLE;
static main_state_t m_state = NORMAL;
static bool m_run = true;

/** @brief Prints instructions to screen in CLI UI.
 */
void print_help(void)
{
    printf("\n\n******* NPE GEM Host Reference Design ********** \n\n\
'h' - help (this menu)\n\
'v' - get version\n\
'a' - start advertising\n\
's' - stop advertising\n\
'i' - goto IDLE\n\
'u' - goto IN-USE\n\
'p' - goto PAUSED\n\
'f' - goto FINISHED\n\
'+' - increase grade\n\
'-' - decrease grade\n\
'd' - start ANT HRM discovery\n\
'n' - enable NFC Reader\n\
'b' - initiate serial/usb bootloader\n\
'q' - quit\n\
\n************************************************* \n");
}

/** @brief Prints instructions to screen in CLI UI.
 */
void print_help_bootloader(void)
{
    printf("\n\n******* Bootloader Help Menu ********** \n\n\
'h' - help (this menu)\n\
'u' - start firmware update process\n\
'q' - quit\n\
\n************************************************* \n");
}

/** @brief Updates simulation data and sends to GEM. This function can 
 * be called in any context, e.g. 1s timer thread and main. All tx call
 * are marshalled to the tx thread. 
 *
 */
void send_data_to_gem(void)
{
    if( m_equipment_state == WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE || 
        m_equipment_state == WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_PAUSED) 
    {
        m_workout_data.speed_centi_kph = 2400; // 24 km/hour
        m_workout_data.distance_meters += 7; // 7 m/s
        m_workout_data.pace = 375;
        m_workout_data.average_pace = 375*1000;
        wf_gem_hci_manager_gymconnect_set_workout_data_elapsed_workout_time(m_workout_data.workout_time_in_seconds++);
        wf_gem_hci_manager_gymconnect_set_workout_data_speed(m_workout_data.speed_centi_kph);
        wf_gem_hci_manager_gymconnect_set_workout_data_current_pace(m_workout_data.pace);
        wf_gem_hci_manager_gymconnect_set_workout_data_average_pace(m_workout_data.average_pace);

        wf_gem_hci_manager_gymconnect_set_workout_data_grade(m_workout_data.deci_grade);
        wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_horizontal_distance(m_workout_data.distance_meters);
        wf_gem_hci_manager_gymconnect_set_workout_data_cumulative_energy(m_workout_data.kcal_total);
        wf_gem_hci_manager_gymconnect_set_workout_data_energy_rate(m_workout_data.rate_kcal_per_hour);
        
        standard_response_t response;
        uint32_t res = npe_hci_library_send_command_gymconnect_perform_workout_data_update(&response);
        assert(res == NPE_GEM_RESPONSE_OK);
        NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response.error_code, NPE_GEM_HCI_LIB_GET_GYMCONNECT_WORKOUT_DATA_UPDATE_ERROR_CODE_STR(response.error_code));
    }
}

/** @brief Handles Events from the GEM. This is called from RX thread, so any heavy processing should be 
 * marshalled to another thread such as main. Any TX calls are automatically marshalled to the TX thread so 
 * TX calls are safe to do from here. 
 */ 
void on_gem_event(gem_event_t* event)
{
    switch(event->message_class.type)
    {
        case WF_GEM_HCI_MSG_CLASS_SYSTEM:
        {
            switch(event->event_id.system_event_type)
            {
                case WF_GEM_HCI_EVENT_ID_SYSTEM_POWER_UP:
                case WF_GEM_HCI_EVENT_ID_SYSTEM_SHUTDOWN:
                {
                    printf("Main: System Event %d Receieved\n", event->event_id.system_event_type); fflush(stdout);
                    break;
                }
                case WF_GEM_HCI_EVENT_ID_SYSTEM_BOOTLOADER_INITIATED:
                {
                    //npe_gem_hci_library_interface_shutdown();
                    printf("*** Bootloader entered ***\n");
                    m_state = BOOTLOADER;
                    print_help_bootloader();
                    break;
                }
                
                default:
                {
                    printf("Main Unknown System Event ID Recieved.\n");fflush(stdout);
                    break;
                }

            }
            break;
        }
        case WF_GEM_HCI_MSG_CLASS_BT_RECEIVER:
        {
            switch(event->event_id.ble_receiver_event_type)
            {
                case WF_GEM_HCI_EVENT_ID_BT_RECEIVER_IBEACON_DISCOVERED:
                {
                    printf("Main: Beacon Discovered\n"); fflush(stdout);
                    break;
                }
                
                default:
                {
                    printf("Main Unknown BT Receiver Event ID..\n");fflush(stdout);
                    break;
                }

            }
            break;
        }
        default:
        {
            printf("Main Unknown Event Class Recieved.\n");fflush(stdout);
        }
    }
}

void DFUProgressHandler(int progress)
{
    printf("Progress %d\n", progress);

    if(progress == 100)
    {
        sleep(1);
        m_state = NORMAL;
        print_help();
    }

}

void DFUTransferTypeHandler(int type)
{
    printf("Transfer type %d\n", type);
}

void process_input_bootloader_state(char input_char)
{

    
    switch(input_char)
    {
        case 'u':
        case 'U':
        {
            npe_gem_hci_library_interface_shutdown();
            sleep(5);
            int ret = npe_hci_dfu_start_update(mp_port, mp_dfu_filename, false, DFUProgressHandler, DFUTransferTypeHandler);
            printf("Serial Update Complete. Status: %d\n", ret);
            m_state = NORMAL;
            break;
        }
        case 'q':
        case 'Q':
        {
            m_run = false;     
            break;
        }
        case 'h':
        case 'H':
        {
            print_help_bootloader();
            break;
        }
        default:
        {
            break;
        }
    }
}

void process_input_normal_state(char input_char)
{
    standard_response_t response_code;
    uint32_t err;
    switch(input_char)
    {

        case 'v':
        case 'V':
        {
            // Request the version
            err = npe_hci_library_send_command_system_get_version(&response_code);
            assert(err == NPE_GEM_RESPONSE_OK);


            printf("Vendor ID: 0x%04x\n", response_code.args.system_version.gem_version.vendor_id);
            printf("Product ID: 0x%04x\n", response_code.args.system_version.gem_version.product_id);
            printf("HW Version ID: %d\n", response_code.args.system_version.gem_version.hw_version);
            printf("Firmware Version: %d.%d.%d\n", response_code.args.system_version.gem_version.fw_version_major, response_code.args.system_version.gem_version.fw_version_minor, response_code.args.system_version.gem_version.fw_version_build);
            printf("Firmware Simple: %d\n", response_code.args.system_version.gem_version.fw_version_simple);
            printf("Bootloader Version: %d\n", response_code.args.system_version.gem_version.bl_version_major);
            printf("Bootloader Revision:%d\n", response_code.args.system_version.gem_version.bl_version_revision);
            printf("Bootloader Vendor: 0x%04x\n", response_code.args.system_version.gem_version.bl_version_vendor);
            printf("Bootloader Device Variant: %d\n", response_code.args.system_version.gem_version.bl_version_device_variant);

            //NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_CONTROL_ADV_START_ERROR_CODE_STR(response_code.error_code));
            break;            
        }
        case 'a':
        case 'A':
        {
            // Start advertising
            err = npe_hci_library_send_command_bluetooth_control_start_advertising(&response_code);
            assert(err == NPE_GEM_RESPONSE_OK);
            NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_CONTROL_ADV_START_ERROR_CODE_STR(response_code.error_code));
            break;            
        }
        case 'b':
        case 'B':
        {
            if(mp_dfu_filename == NULL)
            {
                printf("No DFU filename supplied on program start.\n");
                break;
            }

            // INitiate Bootloader
            npe_hci_library_send_command_system_initiate_bootloader(0, &response_code);
            break;            
        }
        
        case 's':
        case 'S':
        {
            // Stop advertising
            err = npe_hci_library_send_command_bluetooth_control_stop_advertising(&response_code);
            assert(err == NPE_GEM_RESPONSE_OK);
            NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_CONTRL_ADV_STOP_ERROR_CODE_STR(response_code.error_code));
            break;            
        }
        case 'i':
        case 'I':
        {
            // Go to IDLE state
            err = npe_hci_library_send_command_gymconnect_set_fe_state(WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IDLE, &response_code);
            assert(err == NPE_GEM_RESPONSE_OK);
            m_equipment_state = WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IDLE;
            break;
        }
        case 'u':
        case 'U':
        {
            // Go to IN-USE state
            err = npe_hci_library_send_command_gymconnect_set_fe_state(WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE, &response_code);
            assert(err == NPE_GEM_RESPONSE_OK);
            m_equipment_state = WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IN_USE;
            break;

        }
        case 'p':
        case 'P':
        {
            // Go to PAUSE state
            err = npe_hci_library_send_command_gymconnect_set_fe_state(WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_PAUSED, &response_code);
            assert(err == NPE_GEM_RESPONSE_OK);
            m_equipment_state = WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_PAUSED;
            break;
        }
        case 'f':
        case 'F':
        {
            // Go to FINISH state
            err = npe_hci_library_send_command_gymconnect_set_fe_state(WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_FINISHED, &response_code);
            assert(err == NPE_GEM_RESPONSE_OK);
            m_equipment_state = WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_FINISHED;
            break;
        }
        case 'q':
        case 'Q':
        {
            m_run = false;     
            break;
        }
        case '+':
        {
            // Increase grade      
            m_workout_data.deci_grade += 50;

            if(m_workout_data.deci_grade > 1000)
                m_workout_data.deci_grade = 1000;

            break;
        }
        case '-':
        {
            // Decrease grade
            m_workout_data.deci_grade -= 50;
            break;
        }
        case 'h':
        case 'H':
        {
            print_help();
            break;
        }
        case 'd':
        case 'D':
        {
            // Start ANT+ discoveryss
            err =  npe_hci_library_send_command_ant_receiver_start_discovery(120, 0, 30, &response_code);
            assert(err == NPE_GEM_RESPONSE_OK);
            NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_ANT_RECEIVER_START_DISCOVERY_ERROR_CODE_STR(response_code.error_code));
            break;    
        }
        case 'e':
        case 'E':
        {
            // Stop ANT+ discoveryss
            err =  npe_hci_library_send_command_ant_receiver_stop_discovery(120, &response_code);
            assert(err == NPE_GEM_RESPONSE_OK);
            NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_ANT_RECEIVER_STOP_DISCOVERY_ERROR_CODE_STR(response_code.error_code));
            break;    
        }
        case 'n':
        case 'N':
        {
            // Enable NFC reader
            err =  npe_hci_library_send_command_nfc_reader_radio_enable(&response_code);
            assert(err == NPE_GEM_RESPONSE_OK);
            NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_NFC_READER_ERROR_CODE_STR(response_code.error_code));
            break;    
        }
        case 'w':
        case 'W':
        {
            uint16_t company_id = 0x0000; // Nordic BT Sig assigned.
            uint8_t uuid[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            int8_t min_rssi = -100; // Any
            uint8_t timeout = 0; // Infinite
            
            // Start scanning for iBeacon
            err = npe_hci_library_send_command_bt_receiver_start_discovery_ibeacon(company_id, uuid, min_rssi, timeout, &response_code); 
            assert(err == NPE_GEM_RESPONSE_OK);
            NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_BT_RECEIVER_START_DISCOVERY_ERROR_CODE_STR(response_code.error_code));
            break;  
        }
        case 'x':
        case 'X':
        {   
            // Start scanning for iBeacon
            err = npe_hci_library_send_command_bt_receiver_stop_discovery(0x0000, &response_code); 
            assert(err == NPE_GEM_RESPONSE_OK);
            NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_BT_RECEIVER_START_DISCOVERY_ERROR_CODE_STR(response_code.error_code));
            printf("Service ID: 0x%04x\n", response_code.args.bt_receiver_stop_discovery.service_id);

            break;  

        }
        default:
        {           
            break;
        }
    }
}



int main(int argc, char *argv[]) 
{ 

    if(argc > 1)
        mp_port = argv[1];
    if(argc > 2)
        mp_dfu_filename = argv[2];

    standard_response_t response_code;
    uint32_t err;
    
    m_state = NORMAL;
    m_run = true;

    bool waitForPingResponse = true;
    // TODO Handle GEM events
    err = npe_gem_hci_library_interface_init(mp_port, send_data_to_gem, on_gem_event);

    switch(err)
    {
        case NPE_GEM_RESPONSE_SERIAL_NO_COMPORT:
        {
            printf("Comport %s does not exist.\n", mp_port);
            exit(1);
            break;
        }
        case NPE_GEM_RESPONSE_SERIAL_OPEN_FAIL:
        {
            printf("Unable to open port %s.\n", mp_port);
            exit(1);
            break;
        }
        case NPE_GEM_RESPONSE_SERIAL_CONFIG_FAIL:
        {
            printf("Port %s found and opened but unable to configure.\n", mp_port);
            exit(1);
            break;
        }
        default:
        {
            assert(err == NPE_GEM_RESPONSE_OK);
            break;
        }
    }
    

    // Keep sending Ping message until we get a response. 
    while(waitForPingResponse) 
    {
        printf("Ping\n"); fflush(stdout);
        sleep(1);

        err = npe_hci_library_send_ping();

        if(err == NPE_GEM_RESPONSE_OK)
            waitForPingResponse = false;  
        else if(err == NPE_GEM_RESPONSE_RETRIES_EXHAUSTED)
            printf("Retries waiting for response exhausted\n");
        else if(err == NPE_GEM_RESPONSE_TIMEOUT_OUT)
            printf("Wait for response timed out\n");      
        fflush(stdout); 
    }

    // Set Bluetooth name. 
    err = npe_hci_library_send_command_bluetooth_config_set_device_name("GEM3 Reference Design", &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_CONFIG_SET_DEVICE_NAME_ERROR_CODE_STR(response_code.error_code));
            
    err = npe_hci_library_send_command_bluetooth_info_set_manufacturer_name("North Pole Engineering", &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_MANUFACTURER_NAME_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_bluetooth_info_set_model_number("1", &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_MODEL_NUMBER_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_bluetooth_info_set_serial_number("0234", &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_SERIAL_NUMBER_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_bluetooth_info_set_hardware_rev("2", &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_HARDWARE_REVISION_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_bluetooth_info_set_firmware_rev("3", &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_FIRMWARE_REVISION_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_bluetooth_info_set_battery_included(WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_NOT_INCLUDE, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_BATTERY_SERVICE_INCLUDED_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_gymconnect_set_supported_equipment_control_features(0, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_SUPPORTED_EQUIPMENT_CONTROL_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_ant_config_set_hardware_version(1, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_HARDWARE_VERSION_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_ant_config_set_model_number(15000, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_MODEL_NUMBER_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_ant_config_set_software_version(1, 1, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_SOFTWARE_VERSION_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_ant_config_set_serial_number(1234, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_SERIAL_NUMBER_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_ant_config_get_ant_device_number(&response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONFIG_GET_ANT_DEVICE_NUMBER_ERROR_CODE_STR(response_code.error_code));
    printf("ANT Device Number Type: 0x%02x Device Number: 0x%08x\n", response_code.args.ant_config_set_device_number.device_number_type, response_code.args.ant_config_set_device_number.device_number);

    err = npe_hci_library_send_command_ant_config_set_ant_device_number(0x01, 0x00021234, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_ANT_DEVICE_NUMBER_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_ant_config_get_ant_device_number(&response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONFIG_GET_ANT_DEVICE_NUMBER_ERROR_CODE_STR(response_code.error_code));
    printf("ANT Device Number Type: 0x%02x Device Number: 0x%08x\n", response_code.args.ant_config_set_device_number.device_number_type, response_code.args.ant_config_set_device_number.device_number);





    err = npe_hci_library_send_command_gymconnect_set_fe_type(WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_TYPE_TREADMILL, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_FITNESS_EQUIPMENT_TYPE_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_gymconnect_set_fe_state(WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_STATE_IDLE, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_FITNESS_EQUIPMENT_STATE_ERROR_CODE_STR(response_code.error_code));

    // Disable BSCS
    err = npe_hci_library_send_command_bluetooth_info_set_bscs_service_enabled(0, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_BSCS_ENABLED_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_bluetooth_info_get_bscs_service_enabled(&response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    printf("BSCS Enabled: %0d\n", response_code.args.bt_device_info_bsbc_service_get_enabled.enabled);

    assert(0 == response_code.args.bt_device_info_bsbc_service_get_enabled.enabled);

     // Enable BSCS
    err = npe_hci_library_send_command_bluetooth_info_set_bscs_service_enabled(1, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_BSCS_ENABLED_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_bluetooth_info_get_bscs_service_enabled(&response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    printf("BSCS Enabled: %0d\n", response_code.args.bt_device_info_bsbc_service_get_enabled.enabled);

    assert(1 == response_code.args.bt_device_info_bsbc_service_get_enabled.enabled);

    // Disable Power
    err = npe_hci_library_send_command_bluetooth_info_set_power_service_enabled(0, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_POWER_ENABLED_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_bluetooth_info_get_power_service_enabled(&response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    printf("BSCS Enabled: %0d\n", response_code.args.bt_device_info_power_service_get_enabled.enabled);

    assert(0 == response_code.args.bt_device_info_power_service_get_enabled.enabled);

     // Enable Power
    err = npe_hci_library_send_command_bluetooth_info_set_power_service_enabled(1, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_INFO_SET_POWER_ENABLED_ERROR_CODE_STR(response_code.error_code));

    err = npe_hci_library_send_command_bluetooth_info_get_power_service_enabled(&response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    printf("BSCS Enabled: %0d\n", response_code.args.bt_device_info_power_service_get_enabled.enabled);

    assert(1 == response_code.args.bt_device_info_power_service_get_enabled.enabled);

    // Set Frequency Diversity for ANT Profile FEC enabled. Mode = 1 (ASYNC), index A = 9 (2472), index B = 0 (closed)
    err = npe_hci_library_send_command_ant_control_set_ant_profile_frequency_diversity(17, 1, 9, 0, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONTROL_ERROR_CODE_STR(response_code.error_code));


    // Get ANT Profile FEC Frequency Diversity settings
    err = npe_hci_library_send_command_ant_control_get_ant_profile_frequency_diversity(17, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONTROL_ERROR_CODE_STR(response_code.error_code));

    if(response_code.error_code == 0) {
        printf("ANT+ Profile %d Frequency Diversity Settings:\n", response_code.args.ant_control_frequency_diversity_enable.profile);
        printf("Mode: %d\n", response_code.args.ant_control_frequency_diversity_enable.mode);
        printf("Channel A Index: %d\n", response_code.args.ant_control_frequency_diversity_enable.channel_a_frequency_index);
        printf("Channel B Index: %d\n", response_code.args.ant_control_frequency_diversity_enable.channel_b_frequency_index);
        assert(response_code.args.ant_control_frequency_diversity_enable.profile == 17);
        assert(response_code.args.ant_control_frequency_diversity_enable.mode == 1);
        assert(response_code.args.ant_control_frequency_diversity_enable.channel_a_frequency_index == 9);
        assert(response_code.args.ant_control_frequency_diversity_enable.channel_b_frequency_index == 0);
    }


    // Enable ANT Profile FEC Enabled
    err = npe_hci_library_send_command_ant_control_set_ant_profile_enable(17, 1, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_ANT_CONTROL_ERROR_CODE_STR(response_code.error_code));


    // Get ANT Profile FEC Enabled.
    err = npe_hci_library_send_command_ant_control_get_ant_profile_enable(17, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    printf("ANT+ FEC Enabled: %0d\n", response_code.args.ant_control_ant_profile_enable.enable_or_disable);
    assert(1 == response_code.args.ant_control_ant_profile_enable.enable_or_disable);

    //err = npe_hci_library_send_command_bluetooth_control_start_advertising(&response_code);
    //assert(err == NPE_GEM_RESPONSE_OK);
    //NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_GET_BT_CONTROL_ADV_START_ERROR_CODE_STR(response_code.error_code));

//#define DO_HW_PINS
#if defined(DO_HW_PINS)
    err = npe_hci_library_send_command_hardware_get_pin(&response_code);
    assert(err == NPE_GEM_RESPONSE_OK);
    printf("Number of pins configured: %d\n", response_code.args.hw_get_pins.number_of_pins );

    for(int i = 0; i < response_code.args.hw_get_pins.number_of_pins; i++)
    {
        printf("Pin %d, Mode %x\n", response_code.args.hw_get_pins.pin_config[i].pin_number, response_code.args.hw_get_pins.pin_config[i].pin_io_mode);
    }

    npe_hci_pin_t pins[2] = {{3, 0x12}, {5, 0x11}}; // Set pin 2 to be bt adv status output, 3 as bt connected status output

    err = npe_hci_library_send_command_hardware_set_pin(sizeof(pins)/sizeof(npe_hci_pin_t), pins, &response_code);
    assert(err == NPE_GEM_RESPONSE_OK); 
    NPE_GEM_HCI_LIB_PRINT_IF_ERROR(response_code.error_code, NPE_GEM_HCI_LIB_HW_SET_PINS_ERROR_CODE_STR(response_code.error_code));
#endif

    // Send initial data
    send_data_to_gem();

    print_help();
    fflush(stdout);

    while(m_run)
    {
        uint8_t input_char;

        input_char = getchar();

        if(m_state == NORMAL)
            process_input_normal_state(input_char);
        else
        {
            process_input_bootloader_state(input_char);
        }
        

        fflush(stdout);
    }

    npe_gem_hci_library_interface_shutdown();

    exit(0); 
}