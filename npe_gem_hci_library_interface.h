//  Copyright (c) 2017-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
#ifndef __NPE_GEM_HCI_LIBRARY_INTERFACE__
#define __NPE_GEM_HCI_LIBRARY_INTERFACE__
#include <stdint.h>

#include "wf_gem_hci_manager.h"
#include "wf_gem_hci_manager_gymconnect.h"

#define MAX_PINS_ALLOWED        ((uint8_t) 32)


typedef struct {
    uint8_t pin_number;
    uint8_t pin_io_mode;
}npe_hci_pin_t;



/** @brief Union of GEM messages
 *
 */
typedef union 
{
    struct{
        uint8_t bootloader_mode;
    } system_initiate_bootloader;
    struct {
        wf_gem_hci_system_gem_module_version_info_t gem_version;
    } system_version;
    struct{
        uint8_t number_of_pins;
        npe_hci_pin_t pin_config[MAX_PINS_ALLOWED]; 
    } hw_pins;
    
    struct {
       utf8_data_t* bluetooth_name;
    } bt_config_set_device_name;
    

    struct {
       uint16_t fast_advertising_interval;
       uint16_t fast_advertising_timeout;
       uint16_t normal_advertising_interval;
       uint16_t normal_advertising_timeout;
    } bt_config_set_adv_interval_and_timeouts;
    struct {
        utf8_data_t* manufacturer_name;
    } bt_device_info_set_manufacturer_name;
    struct {
        utf8_data_t* model_number;
    } bt_device_info_set_model_number;
    struct {
        utf8_data_t* serial_number;
    }bt_device_info_set_serial_number;
    struct {
        utf8_data_t* hardware_revision;
    }bt_device_info_set_hardware_revision;
    struct {
        utf8_data_t* firmware_revision;
    }bt_device_info_set_firmware_revision;
    struct {
        uint8_t  battery_included;
    }bt_device_info_set_battery_included;
    struct {
        uint8_t vendor_source_id;
        uint16_t vendor_id;
        uint16_t product_id;
        uint16_t product_version;
    } bt_device_info_set_pnp_id;

     struct {
        uint8_t vendor_source_id;
        uint16_t vendor_id;
        uint16_t product_id;
        uint16_t product_version;
    } bt_device_info_get_pnp_id;

    struct {
       uint8_t enabled;
    } bt_device_info_power_service_get_enabled;
    struct {
       uint8_t enabled;
    } bt_device_info_power_service_set_enabled;
    struct {
       uint8_t enabled;
    } bt_device_info_bsbc_service_get_enabled;
    struct {
       uint8_t enabled;
    } bt_device_info_bscs_service_set_enabled;
    struct {
        uint16_t service_id;
        int8_t minimum_rssi;
        uint8_t discovery_timeout;
    }bt_receiver_discovery;
    struct {
        uint16_t service_id;
    }bt_receiver_start_discovery_resp;
    struct {
        uint16_t service_id;
    }bt_receiver_stop_discovery;

    struct {
        uint16_t service_id;
    }bt_receiver_start_discovery;

    struct {
        uint16_t service_id;
        uint8_t bluetooth_address[6];
        uint8_t connection_timeout;
        bool use_gymconnect_events;
    }bt_receiver_connect_device;

    struct {
        uint16_t service_id;        
    }bt_receiver_connect_device_resp;

     struct {
        uint16_t service_id;
        uint8_t bluetooth_address[6];
        uint8_t connection_timeout;
    }bt_receiver_disconnect_device;

    struct {
        uint16_t service_id;        
    }bt_receiver_disconnect_device_resp;

    

    struct {
        uint16_t company_id;
        uint8_t uuid[MAX_STANDARD_LONG_UUID_SIZE];
        uint8_t min_rssi;
        uint8_t discovery_timeout;
    }bt_receiver_start_discovery_ibeacon;
    struct {
        uint32_t  equipment_control_field_identifier;
    }gymconnect_set_supported_equipment_control_features; 
    
    struct {
        uint8_t profile;
        uint8_t enable_or_disable;  
    }ant_control_ant_profile_enable;
    struct {
        uint8_t profile;
        uint8_t mode;
        uint8_t channel_a_frequency_index;
        uint8_t channel_b_frequency_index;  
    }ant_control_frequency_diversity_enable;
    struct {
        uint8_t  hardware_version;
    }ant_config_set_hardware_version;
    struct {
        uint16_t  manufacturer_id;
    }ant_config_manufacturer_id; 
    struct {
        uint16_t  model_number;
    }ant_config_set_model_number; 
    struct {
        uint8_t  main;
        uint8_t supplemental;
    }ant_config_set_version; 
    struct {
        uint32_t serial_number;
    }ant_config_set_serial_number;
    struct {
        uint8_t device_number_type;
        uint32_t device_number;
    }ant_config_set_device_number;
    struct {
        uint16_t ant_plus_device_profile;
        uint8_t prximity_bin;
        uint8_t discovery_timeout;
    }ant_receiver_start_discovery;

    struct {
        uint16_t ant_plus_device_profile;
    }ant_receiver_stop_discovery;

    struct {
        uint16_t ant_plus_device_profile;
        uint32_t device_number;
        uint8_t proximity_bin;
        uint8_t connection_timeout;
    }ant_receiver_connect_device;

    struct {
        uint16_t ant_plus_device_profile;
        uint32_t device_number;
        uint8_t proximity_bin;
        uint8_t connection_timeout;
    }ant_receiver_disconnect_device;

    struct {
        uint16_t ant_plus_device_profile;
        uint8_t attempts;
    }ant_receiver_request_calibration;

    struct {
        uint16_t ant_plus_device_profile;
        wf_gem_hci_ant_receiver_battery_status_data_t data;
    }ant_receiver_request_battery_status;

    struct {
        uint16_t ant_plus_device_profile;
        wf_gem_hci_ant_receiver_get_manufacturer_info_t data;
    }ant_receiver_get_manufacturer_info;

    struct {
        uint16_t scan_period_in_milliseconds;
    }nfc_reader_set_scan_period;

    struct {
        uint16_t tag_types_bitfield;
    }nfc_reader_set_tag_types;

    struct {
        uint8_t mode;
    }nfc_reader_set_radio_test_mode;

    struct {
        wf_gem_hci_nfc_tag_board_configuration_t config;
    }nfc_reader_set_tag_board_config;

    struct {
        uint16_t nfc_reader_supported_tag_types;
    } nfc_reader_supported_tag_types;

    struct {
        uint16_t nfc_reader_scan_period;
    } nfc_reader_scan_period;

    struct {
        wf_gem_hci_gymconnect_fitness_equipment_type_e fe_type;
    }gymconnect_set_fe_type;

    struct {
        wf_gem_hci_gymconnect_fitness_equipment_state_e fe_state;
    }gymconnect_set_fe_state;
    
} npe_inc_function_args;

/** @brief Format of respnse from GEM 
 *
 */
//For pin set/get messages



typedef struct 
{
    uint8_t error_code;
    npe_inc_function_args args;
} standard_response_t;

/** @brief GEM event parameters
 *
 */
typedef union {
    // system_powerup
    // system_shutdown
    // bt_control_advertising_timed_out
    struct {
        uint8_t interface;
    } system_bootloader_initiated;
    struct{
        uint16_t connection_handle;
        uint8_t peer_address_type;
        uint8_t peer_address[BT_ADDRESS_SIZE];
        uint8_t device_type;
    } bt_control_connected;
    struct{
        bool peripheral_solicited;
        bool central_solicited;
    } bt_control_disconnected;

    struct{
        uint16_t service_id;
    } bt_receiver_discovery_timeout;

    struct{
        uint16_t service_id;
        uint8_t bt_address[BT_ADDRESS_SIZE];
    } bt_receiver_device_connected;

    struct{
        uint16_t service_id;
        uint8_t failure_reason;
    } bt_receiver_device_connection_failed;

    struct{
        uint16_t service_id;
        uint8_t disconnect_reason;
    } bt_receiver_device_disconnected;

    struct{
        uint16_t service_id;
        uint8_t bt_address[BT_ADDRESS_SIZE];
        uint8_t local_name_type;
        char local_name[BT_MAX_LOCAL_NAME];
        int8_t rssi;
    } bt_receiver_device_discovered;

    struct{
        wf_gem_hci_bt_receiver_ibeacon_discovered_event_t beacon_info;
    } bt_receiver_ibeacon_discovered;
       struct{
        wf_gem_hci_bt_receiver_heart_rate_data_event_t heart_rate_data;
    } bt_receiver_heart_rate_event;
    

    struct{
        uint16_t ant_plus_profile_id;
        uint32_t device_number;
        int8_t rssi;
    } ant_receiver_device_discovered;
    struct{
        uint16_t ant_plus_profile_id;
    } ant_receiver_discovery_timeout;
    struct{
        uint16_t ant_plus_profile_id;
        uint32_t device_number;
    } ant_receiver_device_connected;
    struct{
        uint16_t ant_plus_profile_id;
        uint8_t disconnect_reason;
    } ant_receiver_device_disconnected;

    struct{
        uint8_t heart_rate;
    } ant_receiver_heart_rate_data;

    struct{
        wf_gem_hci_ant_receiver_power_data_event_t data;
    } ant_receiver_power_data;

    struct{
        uint8_t calibration_response;
        uint8_t auto_zero_support;
        uint8_t auto_zero_enabled;
        uint16_t calibration_data;
    } ant_receiver_power_calibration_response;
    

    
    struct{
        uint16_t ant_plus_profile_id;
    } ant_receiver_device_connected_device_timeout;
    struct{
        uint16_t heart_rate;
        uint16_t avg_heart_rate;
        uint8_t source;
    } gymconnect_event_heart_rate_value;
    struct{
        uint16_t accumulated_total_calories;
        uint16_t accumulated_active_calories;
        uint16_t current_calorie_rate;
        uint8_t source;
    } gymconnect_event_calories_value;
    
    struct{
        uint8_t nfc_tag_type;
        uint8_t nfc_tag_data_type;
        uint16_t payload_length;
        uint8_t payload[MAX_NFC_READ_PAYLOAD];
    } nfc_reader_event_read;

    struct{
        uint8_t TNF_type;
        uint8_t record_type;
        uint16_t record_payload_length;
        uint8_t payload[MAX_NFC_READ_PAYLOAD];
    } nfc_reader_event_NDEF_read;

    uint16_t uint16_value;
} gem_event_args_t;

/** @brief Describes a GEM event in concrete format
 *
 */
typedef struct
{
    union {
        uint32_t id;
        wf_gem_hci_msg_class_e type;
    } message_class;
    union {
        uint32_t id;
        wf_gem_hci_event_id_system_e system_event_type;
        //wf_gem_hci_event_id_hardware_e hardware_event_type;
        wf_gem_hci_event_id_bt_control_e bt_control_event_type;
        //wf_gem_hci_event_id_bt_config_e bt_config_event_type;
        wf_gem_hci_event_id_ble_receiver_e ble_receiver_event_type;
        //wf_gem_hci_event_id_ant_control_e ant_control_event_type;
        //wf_gem_hci_event_id_ant_config_e ant_config_event_type;
        wf_gem_hci_event_id_ant_receiver_e ant_receiver_event_type;
        wf_gem_hci_event_id_nfc_reader_t nfc_reader_event_type;
        wf_gem_hci_event_id_gym_connect_e gym_connect_event_type;
    } event_id;
    gem_event_args_t args;
} gem_event_t;

// Callback function - call @ 1Hz to allow application
// to provide fitness equipment data to the GEM
typedef void (*one_second_timeout_t)(void);

// Callback function - called when an event is
// received from the GEM
typedef void (*on_gem_event_cb_t)(gem_event_t*);

// Callback function - for DFU progress handling
typedef void (*dfu_progress_callback_t)(int);

// Callback function - for DFU transfer type
typedef void (*dfu_transfer_type_callback_t)(int);

/** @brief Initializes the NPE GEM HCI library and serial interface.
 *
 * @param[in] p_comport is a string denoting the name of the port to open.
 * @param[in] one_second_timeout_cb
 * @param[in] gem_event_cb
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_SERIAL_NO_COMPORT
 *          ::NPE_GEM_RESPONSE_SERIAL_OPEN_FAIL
 *          ::NPE_GEM_RESPONSE_SERIAL_CONFIG_FAIL
 */
uint32_t npe_gem_hci_library_interface_init(const char* p_comport, one_second_timeout_t one_second_timeout_cb, on_gem_event_cb_t gem_event_cb);

/** @brief Stops the NPE GEM HCI library running and closes the serial interface. The library is no
 * longer usable after this.
 *
 */
void npe_gem_hci_library_interface_shutdown(void);

/** @brief Send Ping command to GEM.
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_ping(void);

/** @brief Send Get Version command to the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_system_get_version(standard_response_t* p_response);

/** @brief Send Initiate Bootloader command to the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_system_initiate_bootloader(uint8_t bootloader_mode, standard_response_t* p_response);

/** @brief Send Get Pin Configuration command to GEM.
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_hardware_get_pin(standard_response_t* p_response);
uint32_t npe_hci_library_send_command_hardware_set_pin(uint8_t number_of_pins, npe_hci_pin_t p_pin_settings[], standard_response_t* p_set_device_name_response);

/** @brief Send the Bluetooth Device Name to the GEM.
 *
 * @param[in] bluetooth_name is a string denoting the bluetooth device name.
 * @param[out] p_set_device_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_config_set_device_name(utf8_data_t* bluetoothName, standard_response_t* p_set_device_name_response);

/** @brief Get the Bluetooth Advertising Interval and Timeouts
 *
 * @param[out] p_set_device_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_config_get_advertising_intervals_and_timeouts(standard_response_t* p_set_device_name_response);

/** @brief Set the Bluetooth Advertising Interval and Timeouts
 *
 * @param[in] fast_advertising_interval is the fast advertising interval in units of 625us (max is 16384).
 * @param[in] fast_advertising_timeout is the fast advertising timeout in ms. 0 disables timeout
 * @param[in] normal_advertising_interval is the normal advertising interval in units of 625us (max is 16384).
 * @param[in] normal_advertising_timeout is the normal advertising timeout in ms. 0 disables timeout
 * @param[out] p_set_device_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_config_set_advertising_intervals_and_timeouts(
    uint16_t fast_advertising_interval, uint16_t fast_advertising_timout, 
    uint16_t normal_advertising_interval, uint16_t normal_advertising_timeout,
    standard_response_t* p_set_device_name_response);

/** @brief Send the Bluetooth Manufacturer Name to the GEM.
 *
 * @param[in] manufacturer_name is a string denoting the manufacturer name.
 * @param[out] p_set_manufacturer_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_manufacturer_name(utf8_data_t* manufacturer_name, standard_response_t* p_set_manufacturer_name_response);

/** @brief Send the Bluetooth Model Number to the GEM.
 *
 * @param[in] model_number is a string denoting the model number.
 * @param[out] p_set_manufacturer_name_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_model_number(utf8_data_t* model_number, standard_response_t* p_set_model_number_response);

/** @brief Send the Bluetooth Serial Number to the GEM.
 *
 * @param[in] serial_number is a string denoting the serial number.
 * @param[out] p_response_error_code is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_serial_number(utf8_data_t* serial_number, standard_response_t* p_response_error_code);

/** @brief Send the Bluetooth Hardware Revision to the GEM.
 *
 * @param[in] hardware_revision is a string denoting the hardware revision.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_hardware_rev(utf8_data_t* hardware_revision, standard_response_t* p_response);

/** @brief Send the Bluetooth Firmware Revision to the GEM.
 *
 * @param[in] firmware_revision is a string denoting the firmware revision number.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_firmware_rev(utf8_data_t* firmware_revision, standard_response_t* p_response);

/** @brief Send whether battery service should be included to the GEM.
 *
 * @param[in] battery_included 1 byte unsigned value denoting whether to enable battery service.
 *            WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_INCLUDE (0x01)
 *            WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_NOT_INCLUDE (0x00)
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_battery_included(uint8_t battery_included, standard_response_t* p_response);

/** @brief Gets the PnP ID on the GEM.
 * 
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_get_pnp_id(standard_response_t* p_response);

/** @brief Sets the PnP ID on the GEM.
 *
 * @param[in] vendor_source_id 1 byte unsigned value denoting whether id is BT SIG or USB forum assigned.
 *            WF_GEM_HCI_BLUETOOTH_PNP_ID_SIG_COMPANY_ASSIGNED_NUMBER (0x01)
 *            WF_GEM_HCI_BLUETOOTH_PNP_ID_USB_FORUM_ASSIGNED_NUMBER (0x02)
 * @param[in] vendor_id 2 byte unsigned value denoting company's assigned Bluetooth SIG or USB forum assigned vendor ID.
 * @param[in] product_id 2 byte unsigned value denoting company's product ID.
 * @param[in] product_version 2 byte unsigned value denoting company's product version number.
 * 
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_pnp_id(
    uint8_t vendor_source_id,
    uint16_t vendor_id,
    uint16_t product_id,
    uint16_t product_version, 
    standard_response_t* p_response);



/** @brief Gets the BSCS Service Enabled State.
 * 
 * @param[out] p_response contains the enabled state for this service
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_get_bscs_service_enabled(standard_response_t* p_response);

/** @brief Sets the BSCS service enabled state
 *
 * @param[in] enabled 1 byte unsigned value denoting whether the state should e enabled (1) or disabled (0)
 * 
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_bscs_service_enabled(
    uint8_t enabled,
    standard_response_t* p_response);

/** @brief Gets the POWER Service Enabled State.
 * 
 * @param[out] p_response contains the enabled state for this service
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_get_power_service_enabled(standard_response_t* p_response);

/** @brief Sets the POWER service enabled state
 *
 * @param[in] enabled 1 byte unsigned value denoting whether the state should e enabled (1) or disabled (0)
 * 
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 */
uint32_t npe_hci_library_send_command_bluetooth_info_set_power_service_enabled(
    uint8_t enabled,
    standard_response_t* p_response);

/** @brief Starts the discovery process for a bluetooth service.   
 *
 * @param[in] service_id    BT Service ID to start.
 * @param[in] minimum_rssi    RSSI filter for discovery.
 * @param[in] discovery_timeout    Discovery timeout in seconds. 0 disabled the timeout.
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_start_discovery(uint16_t bt_service_id, int8_t minimum_rssi, uint8_t discovery_timeout, standard_response_t* p_response);


/** @brief Stop the discovery process for bluetooth service. Set to 0 for iBoacon  
 *
 * @param[in] service_id    BT Service ID to stop. 0x0000 for iBeacon.
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_stop_discovery(uint16_t service_id, standard_response_t* p_response);

/** @brief Start the discovery process for bluetooth iBeacon  
 *
 * @param[in] company_id        2 byte unsigned value denoting BT SIG assigned company id of the iBeacon.
 * @param[in] p_uuid            Pointer to 16 byte array with uuid of the iBeacon.
 * @param[in] min_rssi          Miniimum RSSI required to pass on disciver iBeacon events
 * @param[in] discovery_timeout Discovery timeout in seconds.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_start_discovery_ibeacon(uint16_t company_id, uint8_t* p_uuid, int8_t min_rssi, uint8_t discovery_timeout, standard_response_t* p_response);


/** @brief Connects to device with service id.
 *
 * @param[in] service_id            BT Service ID to connect to.
 * @param[in] p_bluetooth_address_base64_encoded   Pointer to 6 byte bluetooth address buffer (encoded as base64 string).
 * @param[in] length                length of the base64 encoded bleutooth address string/
 * @param[in] timeout               Connection timeout from 0 - 30s. 0 indicates 30s.
 * 
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_connect_device(uint16_t service_id, uint8_t* p_bluetooth_address_base64_encoded, int length, uint8_t connection_timeout, standard_response_t* p_response);

/** @brief Connects to device with service id, specify what events to use (gymconnect or bluetooth receiver)
 *
 * @param[in] service_id            BT Service ID to connect to.
 * @param[in] p_bluetooth_address_base64_encoded   Pointer to 6 byte bluetooth address buffer (encoded as base64 string).
 * @param[in] length                length of the base64 encoded bluetooth address string/
 * @param[in] timeout               Connection timeout from 0 - 30s. 0 indicates 30s.
 * @param[in] use_gymconnect_events  If true use gymconnect class events instead of bluetooth receiver class..
 * 
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_connect_device_v2(uint16_t service_id, uint8_t* p_bluetooth_address_base64_encoded, int length, uint8_t connection_timeout, bool use_gymconnect_events, standard_response_t* p_response);


/** @brief Disconnects device with service id.
 *
 * @param[in] service_id            BT Service ID to connect to.
 * 
 * @param[out] p_response   is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bt_receiver_disconnect_device(uint16_t service_id, standard_response_t* p_response);

/** @brief Get if specified tx profile is enabled. 
 *
 * @param[in] profile ANT Profile.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_control_get_ant_profile_enable(uint8_t profile, standard_response_t* p_response);

/** @brief Enable or disable ANT+ tx Profile
 *
 * @param[in] profile ANT Profile.
 * @param[in] enable_or_disable Enables or disables the profile.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_control_set_ant_profile_enable(uint8_t profile, uint8_t enable_or_disable, standard_response_t* p_response);


/** @brief Get the ANT profile frequency diversity settings.
 *
 * @param[in] profile ANT Profile.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_control_get_ant_profile_frequency_diversity(uint8_t profile, standard_response_t* p_response);

/** @brief Sets the ANT profile frequency diversity settings.
 *
 * @param[in] profile ANT Profile.
 * @param[in] mode Sync or Async
 * @param[in] channel_a_frequency_index Index of channel A
 * @param[in] channel_b_frequency_index Index of channel B
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_control_set_ant_profile_frequency_diversity(
    uint8_t profile, 
    uint8_t mode,
    uint8_t channel_a_frequency_index,
    uint8_t channel_b_frequency_index,
    standard_response_t* p_response);

/** @brief Get the ANT hardware version  
 *
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_get_hardware_version(standard_response_t* p_response);

/** @brief Send the ANT hardware revision to GEM 
 *
 * @param[in] hardware_version 1 byte unsigned value denoting ANT hardware revision.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_hardware_version(uint8_t hardware_version, standard_response_t* p_response);


/** @brief Get the ANT manufacturer id 
 *
 * @param[out] p_response is the error code recieved from the GEM
 * 
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_get_manufacturer_id(standard_response_t* p_response);


/** @brief Set the ANT manufacturer id 
 *
 * @param[in] manufacturer_id 2 byte unsigned value denoting ANT manufacturer id.
 * @param[out] p_response is the error code recieved from the GEM
 * 
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_manufacturer_id(uint16_t manufacturer_id, standard_response_t* p_response);


/** @brief Send the ANT model number to GEM 
 *
 * @param[in] model_number 2 byte unsigned value denoting ANT model number.
 * @param[out] p_response is the error code recieved from the GEM
 * 
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_model_number(uint16_t model_number, standard_response_t* p_response);

/** @brief Send the ANT software version to GEM 
 *
 * @param[in] main 1 byte unsigned value denoting the main software version.
 * @param[in] main 1 byte unsigned value denoting the supplemental software version.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_software_version(uint8_t main, uint8_t supplemental, standard_response_t* p_response);

/** @brief Send the ANT serial number to GEM 
 *
 * @param[in] serial_number 4 byte unsigned value denoting ANT serial number.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_serial_number(uint32_t serial_number, standard_response_t* p_response);

/** @brief Gets the ANT device number from GEM 
 *
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_get_ant_device_number(standard_response_t* p_response);

/** @brief Sets the ANT device number to GEM 
 *
 * @param[in] device_number_type is the device number type (0 = 16 bit, 1 = 20 bit)
 * @param[in] device_number is the ANT device number
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_config_set_ant_device_number(uint8_t device_number_type, uint32_t device_number, standard_response_t* p_response);


/** @brief Starts ANT+ discovery Process
 *
 * @param[in] ant_plus_profile Is the ANT+ profile to start discovering
 * @param[in] proximity_bin Proximity setting (1-10, 0 is disable)
 * @param[in] discovery_timeout Timeout in seconds - 0 is disable
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_start_discovery(uint16_t ant_plus_profile, uint8_t proximity_bin, uint8_t discovery_timeout, standard_response_t* p_response);

/** @brief Stops ANT+ discovery Process
 *
 * @param[in] ant_plus_profile Is the ANT+ profile to start discovering
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_stop_discovery(uint16_t ant_plus_profile, standard_response_t* p_response);


/** @brief Connect to specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[in] device_number Device Number of Devide to connect to or 0 for wildcard.
 * @param[in] proximity_bin Proximity bin (1-10 or 0 to disable).
 * @param[in] connection_timeout Connection timeout in seconds.
 * @param[out] p_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_connect_device(uint16_t ant_plus_device_profile, uint32_t device_number, uint8_t proximity_bin, uint8_t connection_timeout, standard_response_t* p_response);

/** @brief Disconnect from specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[out] p_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_disconnect_device(uint16_t ant_plus_device_profile, standard_response_t* p_response);



/** @brief Request Calibration from specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[in] attempts Number of times to try sending the request over the air.
 * @param[out] p_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_request_calibration(uint16_t ant_plus_device_profile, uint8_t attempts, standard_response_t* p_response);

/** @brief Get Manufacturer Info from specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[out] p_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_get_manufacturer_info(uint16_t ant_plus_device_profile, standard_response_t* p_response);


/** @brief Request Battery Status from specified ANT device.
 *
 * @param[in] ant_plus_device_profile Device Type to connect to.
 * @param[in] battery_index Battery Index - use 0xFF if unknown
 * @param[out] p_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_ant_receiver_request_battery_status(uint16_t ant_plus_device_profile, uint8_t battery_index, standard_response_t* p_response);

/** @brief Enables the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_radio_enable(standard_response_t* p_response);

/** @brief Disables the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_radio_disable(standard_response_t* p_response);


/** @brief Sets the scan period for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_set_scan_period(uint16_t scan_period_in_milliseconds, standard_response_t* p_response);


/** @brief Gets the scan period for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_get_scan_period(standard_response_t* p_response);


/** @brief Sets the supported tag types for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_set_supported_tag_types(uint16_t tag_type_bitfield, standard_response_t* p_response);

/** @brief Gets the supported tag types for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_get_supported_tag_types(standard_response_t* p_response);

/** @brief Sets the supported tag types for the NFC Reader Radio  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_set_radio_test_mode(uint8_t mode, standard_response_t* p_response);

/** @brief Sets the tag board configuration  
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_set_tag_board_configuration(const wf_gem_hci_nfc_tag_board_configuration_t* p_config, standard_response_t* p_response);


/** @brief Gets the NFC tag board configuration 
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_reader_get_tag_board_configuration(standard_response_t* p_response);

/** @brief NFC Radio Tag Comms Check
 *
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_nfc_tag_board_comms_check(standard_response_t* p_response);

/** @brief Set GEM controllable features
 *
 * @param[in] equipment_control_field_identifier 4 byte unsigned bitfield denoting denoting contollable feautures.
 * @param[out] p_response is the error code recieved from the GEM
 * 
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_gymconnect_set_supported_equipment_control_features(uint32_t equipment_control_field_identifier, standard_response_t* p_response);

/** @brief Send the fitness equipment type to GEM 
 *
 * @param[in] fe_type is an enum denoting the fitness equipment type.
 * @param[out] p_response is the error code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_gymconnect_set_fe_type(wf_gem_hci_gymconnect_fitness_equipment_type_e fe_type, standard_response_t* p_response);

/** @brief Send the fitness equipment state to GEM 
 *
 * @param[in] fe_state is an enum denoting the fitness equipment state.
 * @param[out] p_fe_state_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_gymconnect_set_fe_state(wf_gem_hci_gymconnect_fitness_equipment_state_e fe_state, standard_response_t* p_fe_state_response);

/** @brief Sends set fitness equipment data to the GEM
 *
 * @param[out] p_update_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_gymconnect_perform_workout_data_update(standard_response_t* p_update_response);

/** @brief Starts bluetooth advertising on the GEM
 *
 * @param[out] p_advertising_start_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_control_start_advertising(standard_response_t* p_advertising_start_response);



/** @brief Stops bluetooth advertising on the GEM
 *
 * @param[out] p_advertising_stop_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_control_stop_advertising(standard_response_t* p_advertising_stop_response);

/** @brief Disconnects a Central Device from the GEM
 *
 * @param[out] p_disconnect_central_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_RETRIES_EXHAUSTED
 *          ::NPE_GEM_RESPONSE_TIMEOUT_OUT
 */
uint32_t npe_hci_library_send_command_bluetooth_control_disconnect_central(standard_response_t* p_disconnect_central_response);

/** @brief Starts DFU process for device. Does NOT return until process is complete.
 *
 * @param[out] p_update_response is the response code recieved from the GEM
 *
 * @return  ::NPE_GEM_RESPONSE_OK
 *          ::NPE_GEM_RESPONSE_INVALID_PARAMETER
 *          ::NPE_GEM_RESPONSE_SERIAL_OPEN_FAIL
 *          ::NPE_GEM_RESPONSE_UNABLE_TO_WRITE
 *          ::NPE_GEM_RESPONSE_UNKNOWN_ERROR
 * 
 */
uint32_t npe_hci_dfu_start_update(char* comport, char* zip_package_name, bool is_usb_transport,  dfu_progress_callback_t progress_cb, dfu_transfer_type_callback_t transfer_type_callback);


#endif