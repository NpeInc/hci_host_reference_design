//  Copyright (c) 2012-2020 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#ifndef _NPE_LOG_H_
#define _NPE_LOG_H_

#ifdef __ANDROID__
#  include <android/log.h>
#  define LOG_TAG "GEM HCI native"
#  define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG,__VA_ARGS__)
#  define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , LOG_TAG,__VA_ARGS__)
#  define LOGI(...) __android_log_print(ANDROID_LOG_INFO   , LOG_TAG,__VA_ARGS__)
#  define LOGW(...) __android_log_print(ANDROID_LOG_WARN   , LOG_TAG,__VA_ARGS__)
#  define LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , LOG_TAG,__VA_ARGS__)
#else
#  include <stdio.h>
#  define LOGV(...) printf(__VA_ARGS__);
#  define LOGD(...) printf(__VA_ARGS__);
#  define LOGI(...) printf(__VA_ARGS__);
#  define LOGW(...) printf("  * Warning: "); printf(__VA_ARGS__);
#  define LOGE(...) printf("  *** Error:  ");printf(__VA_ARGS__);
#endif // __ANDROID__

#endif //#ifndef _NPE_LOG_H_
